package moodle

import (
	"encoding/json"
	"strconv"
	"time"
)

type Conversation struct {
	Id                           uint
	Name                         string
	MemberCount                  uint
	IsMuted                      bool
	IsFavourite                  bool
	IsRead                       bool
	UnreadCount                  uint
	Members                      []Member
	Messages                     []Message
	CanDeleteMessagesForAllUsers bool
}

func (c Conversation) Title() string {
	if c.Name != "" {
		return c.Name
	}

	return c.Members[0].FullName
}

type Member struct {
	Id                      uint
	FullName                string
	ProfileURL              string
	ProfileImageURL         string
	ProfileImageURLSmall    string
	IsOnline                *bool
	ShowOnlineStatus        bool
	IsBlocked               bool
	IsContact               bool
	IsDeleted               bool
	CanMessageEvenIfBlocked bool
	CanMessage              bool
	RequiresContact         bool
	ContactRequests         []struct{}
}

type Time time.Time

func (t *Time) UnmarshalJSON(b []byte) error {
	var s int64
	err := json.Unmarshal(b, &s)
	*t = Time(time.Unix(s, 0))
	return err
}

type Message struct {
	Id          uint
	UserIdFrom  uint
	Text        string
	TimeCreated Time
}

type conversationsReq struct {
	UserId string `json:"userid"`
}

type conversationsRes struct {
	Conversations []Conversation
}

func (m *Moodle) Conversations() ([]Conversation, error) {
	err := m.fetchCfg()
	if err != nil {
		return nil, err
	}

	args := conversationsReq{
		UserId: strconv.FormatUint(uint64(m.UserId), 10),
	}
	var res conversationsRes
	err = m.ajax("core_message_get_conversations", args, &res)
	return res.Conversations, err
}
