package moodle

import (
	"fmt"
	"io"
	"net/url"
	"strconv"
	"strings"

	"lelux.net/x/html"
)

type CourseId uint

type Course struct {
	Id              CourseId
	FullName        string
	ShortName       string
	IdNumber        string
	FullNameDisplay string
	ViewURL         string
	CourseCategory  string
}

type courseSearchReq struct {
	Classification string `json:"classification"`
	SearchValue    string `json:"searchvalue"`
}

type courseSearchRes struct {
	Courses []Course `json:"courses"`
}

func (m *Moodle) CourseSearch(query string) ([]Course, error) {
	args := courseSearchReq{
		Classification: "search",
		SearchValue:    query,
	}
	var res courseSearchRes
	err := m.ajax("core_course_get_enrolled_courses_by_timeline_classification", args, &res)
	return res.Courses, err
}

type DownloadSection struct {
	Id    uint
	Title string
	Items []DownloadItem
}

type DownloadItem struct {
	Id    uint
	Type  DownloadType
	Title string
}

type DownloadType uint8

const (
	DownloadResource DownloadType = iota
	DownloadAssign
	DownloadFolder
)

func (t DownloadType) String() string {
	switch t {
	case DownloadResource:
		return "Resource"
	case DownloadAssign:
		return "Assign"
	case DownloadFolder:
		return "Folder"
	default:
		return fmt.Sprintf("%%!DownloadType(%d)", t)
	}
}

func (m *Moodle) DownloadCenter(id CourseId) ([]DownloadSection, error) {
	n, err := m.req(fmt.Sprintf("/local/downloadcenter/index.php?courseid=%d", id), nil)
	if err != nil {
		return nil, err
	}

	var ds []DownloadSection

	f := n.Query(form)
	for c := (*html.Node)(f.FirstChild); c != nil; c = (*html.Node)(c.NextSibling) {
		if c.GetAttr("class") != "card block mb-3" {
			continue
		}

		labels := c.QueryAll(label)
		sl := labels[0]
		idStr := strings.TrimPrefix(sl.GetAttr("for"), "id_item_topic_")
		id, err := strconv.ParseUint(idStr, 10, 32)
		if err != nil {
			return nil, err
		}
		s := DownloadSection{
			Id:    uint(id),
			Title: sl.FirstChild.NextSibling.FirstChild.Data,
			Items: make([]DownloadItem, len(labels)-1)}

		for i, l := range labels[1:] {
			title := l.FirstChild.NextSibling.FirstChild.FirstChild.Data

			forId := l.GetAttr("for")
			forSuffix := strings.TrimPrefix(forId, "id_item_")
			typeStr, idStr, ok := strings.Cut(forSuffix, "_")
			if !ok {
				return nil, fmt.Errorf("unable to parse id '%s'", forId)
			}

			var typ DownloadType
			switch typeStr {
			case "resource":
				typ = DownloadResource
			case "assign":
				typ = DownloadAssign
			case "folder":
				typ = DownloadFolder
			}

			id, err := strconv.ParseUint(idStr, 10, 32)
			if err != nil {
				return nil, err
			}
			s.Items[i] = DownloadItem{Id: uint(id), Type: typ, Title: title}
		}

		ds = append(ds, s)
	}

	return ds, nil
}

func (m *Moodle) DownloadCenterDownload(id CourseId, topic uint, typ DownloadType, item uint) (io.ReadCloser, error) {
	err := m.fetchCfg()
	if err != nil {
		return nil, err
	}

	var typStr string
	switch typ {
	case DownloadResource:
		typStr = "resource"
	case DownloadAssign:
		typStr = "assign"
	case DownloadFolder:
		typStr = "folder"
	}

	values := url.Values{
		"courseid": {strconv.FormatUint(uint64(id), 10)},
		"sesskey":  {m.SessKey},
		"_qf__local_downloadcenter_download_form": {"1"},
		fmt.Sprintf("item_topic_%d", topic):       {"1"},
		fmt.Sprintf("item_%s_%d", typStr, item):   {"1"},
	}

	return m.rawReq("/local/downloadcenter/index.php", values)
}
