package moodle

import (
	"strconv"
	"strings"
	"time"
)

type AssignId uint

type Assign struct {
	FullName   string
	Opened     *time.Time
	Due        *time.Time
	Submission []File
}

type File struct {
	Name string
}

var (
	timeFormat = "Monday, 2 January 2006, 03:04 PM"
)

func (m *Moodle) Assign(id AssignId) (Assign, error) {
	n, err := m.req("/mod/assign/view.php?id="+strconv.FormatUint(uint64(id), 10), nil)
	if err != nil {
		return Assign{}, err
	}

	var assign Assign

	assign.FullName = n.Query(h1).FirstChild.Data

	descN := n.Query(descriptionInner)
	if descN != nil {
		openedWrap := descN.FirstChild.NextSibling
		openedStr := strings.TrimSpace(openedWrap.FirstChild.NextSibling.NextSibling.Data)
		opened, err := time.ParseInLocation(timeFormat, openedStr, m.Timezone)
		if err != nil {
			return Assign{}, err
		}
		assign.Opened = &opened

		dueWrap := openedWrap.NextSibling.NextSibling
		dueStr := strings.TrimSpace(dueWrap.FirstChild.NextSibling.NextSibling.Data)
		due, err := time.ParseInLocation(timeFormat, dueStr, m.Timezone)
		if err != nil {
			return Assign{}, err
		}
		assign.Due = &due
	}

	submission := n.Query(submissionStatusTable)
	files := list{submission.Query(li)}
	for files.Node != nil {
		a := files.Query(a)
		assign.Submission = append(assign.Submission, File{
			Name: a.FirstChild.Data,
		})

		files.Next()
	}

	return assign, nil
}
