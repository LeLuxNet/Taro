package moodle

import (
	"fmt"
	"strconv"
	"strings"

	"lelux.net/x/html"
)

type Grade struct {
	Item   string
	URL    string
	Assign *AssignId

	Grade *float32

	From float32
	To   float32
}

func (m *Moodle) GradeReport(course CourseId) ([]Grade, error) {
	n, err := m.req("/grade/report/user/index.php?id="+strconv.FormatUint(uint64(course), 10), nil)
	if err != nil {
		return nil, err
	}

	table := n.Query(userGrade)

	var grades []Grade

	row := list{table.Query(spacer)}
	for row.Next() {
		class := row.GetAttr("class")
		if strings.HasSuffix(class, " lastrow") {
			break
		} else if strings.HasSuffix(class, " spacer") {
			continue
		}

		col := list{(*html.Node)(row.FirstChild)}
		col.Next()
		if col.Node == nil || col.GetAttr("colspan") == "9" || !strings.Contains(col.GetAttr("class"), " item ") {
			continue
		}

		var grade Grade

		item := col.Query(a)
		grade.URL = item.GetAttr("href")

		_, idStr, ok := strings.Cut(grade.URL, "/mod/assign/view.php?id=")
		if ok {
			id, err := strconv.ParseUint(idStr, 10, 32)
			if err != nil {
				return grades, fmt.Errorf("failed to extract assign id from url '%s'", grade.URL)
			}
			aId := AssignId(id)
			grade.Assign = &aId
		}

		grade.Item = item.FirstChild.Data
		col.Next()

		col.Next()

		if col.FirstChild != nil && col.FirstChild.Data != "-" {
			num, err := strconv.ParseFloat(strings.Replace(col.FirstChild.Data, ",", ".", 1), 32)
			if err != nil {
				return grades, fmt.Errorf("failed to parse grade: %w", err)
			}
			num32 := float32(num)
			grade.Grade = &num32
		}
		col.Next()

		fromStr, toStr, ok := strings.Cut(col.FirstChild.Data, "–")
		if !ok {
			return grades, fmt.Errorf("missing en dash in grade range '%s'", col.FirstChild.Data)
		}
		if fromStr != "" || toStr != "" {
			from, err := strconv.ParseFloat(strings.Replace(fromStr, ",", ".", 1), 32)
			if err != nil {
				return grades, fmt.Errorf("failed to parse grade range from: %w", err)
			}
			grade.From = float32(from)
			to, err := strconv.ParseFloat(strings.Replace(toStr, ",", ".", 1), 32)
			if err != nil {
				return grades, fmt.Errorf("failed to parse grade range to: %w", err)
			}
			grade.To = float32(to)
		}

		grades = append(grades, grade)
	}

	return grades, nil
}
