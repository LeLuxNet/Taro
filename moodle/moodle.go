package moodle

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strconv"
	"strings"
	"time"

	"lelux.net/x/html"
)

type Moodle struct {
	URL      string
	Session  string
	SessKey  string
	UserId   uint
	Timezone *time.Location
}

func (m *Moodle) rawReq(path string, values url.Values) (io.ReadCloser, error) {
	method := http.MethodGet
	var body io.Reader
	if values != nil {
		method = http.MethodPost
		body = strings.NewReader(values.Encode())
	}

	req, err := http.NewRequest(method, m.URL+path, body)
	if err != nil {
		return nil, err
	}

	if values != nil {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}

	req.AddCookie(&http.Cookie{Name: "MoodleSession", Value: m.Session})

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	return res.Body, nil
}

func (m *Moodle) req(path string, values url.Values) (*html.Node, error) {
	body, err := m.rawReq(path, values)
	if err != nil {
		return nil, err
	}
	defer body.Close()

	n, err := html.Parse(body)
	if err != nil {
		return nil, err
	}

	m.extractCfg(n)

	return n, nil
}

func (m *Moodle) extractCfg(n *html.Node) {
	s := n.Query(sesskey)
	if s != nil { // TODO: Extract from M.cfg in script tag
		m.SessKey = s.GetAttr("value")
	}

	userIdStr := n.Query(navNotificationPopoverContainer).GetAttr("data-userid")
	userId, err := strconv.ParseUint(userIdStr, 10, 32)
	if err != nil {
		panic(err)
	} else {
		m.UserId = uint(userId)
	}
}

func (m *Moodle) fetchCfg() error {
	if m.SessKey != "" {
		return nil
	}

	_, err := m.req("/my", nil)
	return err
}

type ajaxReq struct {
	Args       interface{} `json:"args"`
	Index      uint        `json:"index"`
	MethodName string      `json:"methodname"`
}

type ajaxRes struct {
	Error     bool            `json:"error"`
	Exception Exception       `json:"exception"`
	Data      json.RawMessage `json:"data"`
}

type Exception struct {
	Message     string
	ErrorCode   string
	Link        string
	MoreInfoURL string
}

func (e Exception) Error() string {
	return fmt.Sprintf("moodle %s error: %s (%s)", e.ErrorCode, e.Message, e.MoreInfoURL)
}

func (m *Moodle) ajax(name string, req, res interface{}) error {
	err := m.fetchCfg()
	if err != nil {
		return err
	}

	reqB, err := json.Marshal([1]ajaxReq{{
		Args:       req,
		Index:      0,
		MethodName: name,
	}})
	if err != nil {
		return err
	}

	reqH, err := http.NewRequest(http.MethodPost, m.URL+"/lib/ajax/service.php?sesskey="+url.QueryEscape(m.SessKey), bytes.NewReader(reqB))
	if err != nil {
		return err
	}

	reqH.AddCookie(&http.Cookie{Name: "MoodleSession", Value: m.Session})

	resH, err := http.DefaultClient.Do(reqH)
	if err != nil {
		return err
	}
	defer resH.Body.Close()

	resB, err := io.ReadAll(resH.Body)
	if err != nil {
		return err
	}

	var resJ [1]ajaxRes
	err = json.Unmarshal(resB, &resJ)
	if err != nil {
		return err
	}

	if resJ[0].Error {
		return resJ[0].Exception
	}

	return json.Unmarshal(resJ[0].Data, res)
}

func submitForm(c *http.Client, res *http.Response, body url.Values) (*http.Response, error) {
	defer res.Body.Close()

	n, err := html.Parse(res.Body)
	if err != nil {
		return nil, err
	}

	f := n.Query(form)
	action := f.GetAttr("action")
	actionUrl, err := res.Request.URL.Parse(action)
	if err != nil {
		return nil, err
	}

	for _, input := range f.QueryAll(input) {
		name := input.GetAttr("name")
		if body.Has(name) {
			continue
		}
		body.Set(name, input.GetAttr("value"))
	}

	return c.Post(actionUrl.String(), "application/x-www-form-urlencoded", strings.NewReader(body.Encode()))
}

func (m *Moodle) LoginShibboleth(username, password string) error {
	jar, err := cookiejar.New(nil)
	if err != nil {
		return err
	}

	c := &http.Client{Jar: jar}
	res, err := c.Get(m.URL + "/auth/shibboleth/index.php")
	if err != nil {
		return err
	}

	res, err = submitForm(c, res, url.Values{})
	if err != nil {
		return err
	}

	res, err = submitForm(c, res, url.Values{
		"j_username":              {username},
		"j_password":              {password},
		"_shib_idp_revokeConsent": {},
		"_eventId_proceed":        {""},
	})
	if err != nil {
		return err
	}

	res, err = submitForm(c, res, url.Values{})
	if err != nil {
		return err
	}
	defer res.Body.Close()

	for _, c := range jar.Cookies(res.Request.URL) {
		if c.Name == "MoodleSession" {
			m.Session = c.Value

			n, err := html.Parse(res.Body)
			if err != nil {
				return err
			}
			m.extractCfg(n)

			return nil
		}
	}

	return errors.New("unable to find session cookie")
}
