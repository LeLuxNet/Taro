package moodle

import (
	ghtml "golang.org/x/net/html"
	"lelux.net/x/html"
)

var (
	a                               = html.MustCompile("a")
	h1                              = html.MustCompile("h1")
	li                              = html.MustCompile("li")
	form                            = html.MustCompile("form")
	input                           = html.MustCompile("input")
	label                           = html.MustCompile("label")
	spacer                          = html.MustCompile(".spacer")
	descriptionInner                = html.MustCompile(".description-inner")
	submissionStatusTable           = html.MustCompile(".submissionstatustable")
	userGrade                       = html.MustCompile(".user-grade")
	sesskey                         = html.MustCompile("[name=sesskey]")
	navNotificationPopoverContainer = html.MustCompile("#nav-notification-popover-container")
)

type list struct {
	*html.Node
}

func (t *list) Next() bool {
	for t.Node != nil {
		t.Node = (*html.Node)(t.NextSibling)
		if t.Node != nil && t.Type == ghtml.ElementNode {
			return true
		}
	}
	return false
}

func (t *list) Yield() *html.Node {
	t.Next()
	return t.Node
}
