package friedolin

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

var (
	client = http.Client{CheckRedirect: func(req *http.Request, via []*http.Request) error {
		return http.ErrUseLastResponse
	}}
)

type Fridolin struct {
	Language string

	Session string
	Asi     string
}

func (f *Fridolin) Login(username, password string) error {
	_, err := f.user(true, 1, url.Values{
		"asdf": {username},
		"fdsa": {password},
	})
	if err != nil {
		if err2, ok := err.(UserError); ok {
			switch err2.Message {
			case "You entered a combination of login and password that does not match. Please try again.", "Login und/oder Passwort sind nicht korrekt. Bitte versuchen Sie es erneut.":
				return ErrWrongCredentials
			}
		}
		return err
	}

	// The next request being sent fast enough causes the server to think we're still logged out
	time.Sleep(500 * time.Millisecond)

	return nil
}

func (f *Fridolin) fetchAsi() error {
	n, err := f.user(false, 0, nil)
	if err != nil {
		return err
	}

	return f.extractAsi(n)
}

func (f *Fridolin) Logout() error {
	_, err := f.user(false, 4, nil)
	f.Session = ""
	f.Asi = ""
	return err
}

type Season uint8

const (
	Summer Season = 1 + iota
	Winter
)

func (s Season) String() string {
	switch s {
	case Winter:
		return "Winter"
	case Summer:
		return "Summer"
	default:
		return fmt.Sprintf("%%!Season(%d)", s)
	}
}

func (s Season) MarshalText() ([]byte, error) {
	switch s {
	case Winter:
		return []byte("winter"), nil
	case Summer:
		return []byte("summer"), nil
	default:
		return nil, fmt.Errorf("unknown season %d", s)
	}
}

type Term struct {
	Year   uint16 `json:"year"`
	Season Season `json:"season"`
}

func (t Term) Before(t2 Term) bool {
	return t.Year < t2.Year || (t.Year == t2.Year && t.Season < t2.Season)
}

func (t Term) After(t2 Term) bool {
	return t.Year > t2.Year || (t.Year == t2.Year && t.Season > t2.Season)
}

func (t Term) Compare(u Term) int {
	if t.Year < u.Year {
		return -1
	} else if t.Year > u.Year {
		return +1
	}

	if t.Season < u.Season {
		return -1
	} else if t.Season > u.Season {
		return +1
	} else {
		return 0
	}
}

func (t Term) id() uint16 {
	return t.Year*10 + uint16(t.Season)
}

func ParseTerm(s string) (Term, error) {
	i := strings.LastIndexByte(s, ' ')
	if i == -1 {
		return Term{}, errors.New("missing space")
	}

	var season Season
	switch s[:i] {
	case "WS", "Winter", "Winter term", "Wintersem.", "Wintersemester":
		season = Winter
	case "SS", "SoSe", "Summer", "Sommer", "Summer term", "Sommersem.", "Sommersemester":
		season = Summer
	default:
		return Term{}, fmt.Errorf("unknown season: '%s'", s[:i])
	}

	right := s[i+1:]
	i = strings.LastIndexByte(right, '/')
	if i != -1 {
		right = right[:i]
	}

	year, err := strconv.ParseUint(right, 10, 16)
	if err != nil {
		return Term{}, fmt.Errorf("failed to parse year: %w", err)
	}
	if year < 100 {
		year += 2000
	}

	return Term{Season: season, Year: uint16(year)}, nil
}

type TermCycle uint8

const (
	NoRepetition = iota
	OnlySummer
	OnlyWinter
	Every
)

func (c TermCycle) MarshalText() ([]byte, error) {
	switch c {
	case NoRepetition:
		return []byte("noRepetition"), nil
	case OnlySummer:
		return []byte("onlySummer"), nil
	case OnlyWinter:
		return []byte("onlyWinter"), nil
	case Every:
		return []byte("every"), nil
	default:
		return nil, fmt.Errorf("unknown term cycle %d", c)
	}
}

func (c TermCycle) Contains(s Season) bool {
	return c&TermCycle(s) != 0
}
