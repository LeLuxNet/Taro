package friedolin

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"lelux.net/taro"
)

type Frequency uint8

const (
	Once Frequency = iota
	Weekly
	Fortnightly
)

func (f Frequency) String() string {
	switch f {
	case Once:
		return "Once"
	case Weekly:
		return "Weekly"
	case Fortnightly:
		return "Fortnightly"
	default:
		return fmt.Sprintf("%%!Frequency(%d)", f)
	}
}

func (f Frequency) MarshalText() ([]byte, error) {
	switch f {
	case Once:
		return []byte("once"), nil
	case Weekly:
		return []byte("weekly"), nil
	case Fortnightly:
		return []byte("fortnightly"), nil
	default:
		return nil, fmt.Errorf("unknown frequency %d", f)
	}
}

type Time struct {
	Hour   uint8 `json:"hour"`
	Minute uint8 `json:"minute"`
}

func parseTime(s string) (Time, error) {
	hourStr, minuteStr, ok := strings.Cut(s, ":")
	if !ok {
		return Time{}, errors.New("missing colon")
	}

	hour, err := strconv.ParseUint(hourStr, 10, 8)
	if err != nil {
		return Time{}, fmt.Errorf("hour %w", err)
	}
	minute, err := strconv.ParseUint(minuteStr, 10, 8)
	if err != nil {
		return Time{}, fmt.Errorf("minute %w", err)
	}
	return Time{Hour: uint8(hour), Minute: uint8(minute)}, nil
}

func parseDate(s string) (taro.Date, error) {
	return taro.ParseDate("02.01.2006", s)
}
