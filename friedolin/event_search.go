package friedolin

import (
	"fmt"
	"log"
	"net/url"
	"strconv"
	"strings"
	"time"

	"lelux.net/x/html"
)

type EventSearch struct {
	Term   Term
	Number EventId
	Name   string
	Type   EventType

	Weekday  *time.Weekday
	FromTime *Time
	ToTime   *Time
}

type SearchEvent struct {
	Id     EventId
	Type   EventType
	Number uint32
	Name   string
}

func (f *Fridolin) SearchEvents(s EventSearch, count, offset uint) ([]SearchEvent, error) {
	params := url.Values{
		"search":   {"1"},
		"P.start":  {strconv.FormatUint(uint64(offset), 10)},
		"P.anzahl": {strconv.FormatUint(uint64(count), 10)},
	}

	if s.Term.Year != 0 {
		params.Set("veranstaltung.semester", strconv.FormatUint(uint64(s.Term.id()), 10))
	}
	if s.Number != 0 {
		params.Set("veranstaltung.veranstnr", strconv.FormatUint(uint64(s.Number), 10))
	}
	if s.Name != "" {
		params.Set("veranstaltung.dtxt", s.Name)
	}
	if s.Type != 0 {
		params.Set("veranstaltung.verartid", strconv.FormatUint(uint64(s.Type), 10))
	}
	if s.Weekday != nil {
		var val string
		switch *s.Weekday {
		case time.Monday:
			val = "6"
		case time.Tuesday:
			val = "2"
		case time.Wednesday:
			val = "5"
		case time.Thursday:
			val = "3"
		case time.Friday:
			val = "4"
		case time.Saturday:
			val = "7"
		case time.Sunday:
			val = "8"
		default:
			return nil, fmt.Errorf("unknown weekday: %s", *s.Weekday)
		}
		params.Set("k_wochentag.wochentagid", val)
	}
	if s.FromTime != nil {
		params.Set("veransttermin.beginn", fmt.Sprintf("%02d:%02d", s.FromTime.Hour, s.FromTime.Minute))
	}
	if s.ToTime != nil {
		params.Set("veransttermin.ende", fmt.Sprintf("%02d:%02d", s.ToTime.Hour, s.ToTime.Minute))
	}

	n, err := f.htmlReq(false, "wsearchv", params)
	if err != nil {
		return nil, err
	}

	var res []SearchEvent
	table := trTable(n.Query(table_))
	for table.Next() {
		var e SearchEvent

		numberField := table.FirstChild.NextSibling
		numberStr := strings.TrimSpace(numberField.FirstChild.Data)
		if numberStr != "" {
			number, err := strconv.ParseUint(numberStr, 10, 32)
			if err != nil {
				return res, err
			}
			e.Number = uint32(number)
		}

		eventField := numberField.NextSibling.NextSibling
		a := (*html.Node)(eventField.FirstChild.NextSibling)
		e.Name = a.FirstChild.Data
		url, err := url.Parse(a.GetAttr("href"))
		if err != nil {
			return res, err
		}
		id, err := strconv.ParseUint(url.Query().Get("publishid"), 10, 32)
		if err != nil {
			return res, err
		}
		e.Id = EventId(id)

		typeField := eventField.NextSibling.NextSibling
		e.Type, err = parseEventType(strings.TrimSpace(typeField.FirstChild.Data))
		if err != nil {
			log.Println(err)
		}

		res = append(res, e)
	}

	return res, nil
}
