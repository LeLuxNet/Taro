package friedolin

import "fmt"

type EventType int16

const (
	TrainingCourse EventType = iota + 1
	Proseminar
	MainSeminar
	Exercise
	_
	Colloquium
	AdvancedSeminar
	_
	_
	_
	Project
	_
	Propaedeutic
	SeminarExercise
	_
	FieldExercise
	_ // Module
	_
	Tutorial
	Excursion
	_
	TrainingResearchProject
	OptionalLecture
	OptionalSeminar
	_
	_
	Teleteaching
	Consulting
	Miscellaneous
	_
	WorkingGroup
	_ //
	RevisionCourse
	_
	Workshop
	_
	LectureExercise
	LectureSeminar
	_
	_
	_
	BasicModule
	_
	_
	_
	PracticalWorkSeminar
	_
	LecturePracticalWork
	_
	PracticeModule
	AdvancedModule
	_
	LanguageCourse
	_
	_ //
	_
	_
	_
	_
	_
	_
	PracticalWork
)

const (
	Seminar EventType = 113

	Lecture     EventType = 236
	BlockCourse EventType = 247
)

const (
	Course EventType = 324 + iota
	_
	_
	_
	_
	CrosssectionalArea
	_
	_
	_
	_ //
	_
	MovieScreening
	_
	Exam
	Conference
	ConferenceSymposium
	MusicalEvent
	_
	_
	_
	Meeting_
	Talk
	_
	LectureSeries
	_
	Session
	OtherEvent
	CelebrationFestivity
	Convention
	ExamTest
	ServiceTime
	Exhibition
	PracticalSemesterAccompanyingEvent
	Training
	Tutorial2
	Workshop2
	Tour
	Speech
	_
	InformationSession
	Course2
	OnlineSeminar
	SeminarExcursion
	OnlineLecture
	BasicCourse
	Sample
	_ //
	_ //
	SummerSchool
	InauguralLecture
	_ //
	Blockage
	_
	Exercises

	Consultation EventType = -1
	RepeatExam   EventType = -2
)

func (e EventType) MarshalText() ([]byte, error) {
	switch e {
	case TrainingCourse:
		return []byte("trainingCourse"), nil
	case Proseminar:
		return []byte("proseminar"), nil
	case MainSeminar:
		return []byte("mainSeminar"), nil
	case Exercise:
		return []byte("exercise"), nil
	case Colloquium:
		return []byte("colloquium"), nil
	case AdvancedSeminar:
		return []byte("advancedSeminar"), nil
	case Project:
		return []byte("project"), nil
	case Propaedeutic:
		return []byte("propaedeutic"), nil
	case SeminarExercise:
		return []byte("seminarExercise"), nil
	case FieldExercise:
		return []byte("fieldExercise"), nil
	case Tutorial:
		return []byte("tutorial"), nil
	case Excursion:
		return []byte("excursion"), nil
	case TrainingResearchProject:
		return []byte("trainingResearchProject"), nil
	case OptionalLecture:
		return []byte("optionalLecture"), nil
	case OptionalSeminar:
		return []byte("optionalSeminar"), nil
	case Teleteaching:
		return []byte("teleteaching"), nil
	case Consulting:
		return []byte("consulting"), nil
	case Miscellaneous:
		return []byte("miscellaneous"), nil
	case WorkingGroup:
		return []byte("workingGroup"), nil
	case RevisionCourse:
		return []byte("revisionCourse"), nil
	case Workshop:
		return []byte("workshop"), nil
	case LectureExercise:
		return []byte("lectureExercise"), nil
	case LectureSeminar:
		return []byte("lectureSeminar "), nil
	case BasicModule:
		return []byte("basicModule"), nil
	case PracticalWorkSeminar:
		return []byte("practicalWorkSeminar"), nil
	case LecturePracticalWork:
		return []byte("lecturePracticalWork"), nil
	case PracticeModule:
		return []byte("practiceModule"), nil
	case AdvancedModule:
		return []byte("advancedModule"), nil
	case LanguageCourse:
		return []byte("languageCourse"), nil
	case PracticalWork:
		return []byte("practicalWork"), nil

	case Seminar:
		return []byte("seminar"), nil
	case Lecture:
		return []byte("lecture"), nil
	case BlockCourse:
		return []byte("blockCourse"), nil

	case Course:
		return []byte("course"), nil
	case CrosssectionalArea:
		return []byte("crosssectionalArea"), nil
	case MovieScreening:
		return []byte("movieScreening"), nil
	case Exam:
		return []byte("exam"), nil
	case Conference:
		return []byte("conference"), nil
	case ConferenceSymposium:
		return []byte("conferenceSymposium"), nil
	case MusicalEvent:
		return []byte("musicalEvent"), nil
	case Meeting_:
		return []byte("meeting"), nil
	case Talk:
		return []byte("talk"), nil
	case LectureSeries:
		return []byte("lectureSeries"), nil
	case Session:
		return []byte("session"), nil
	case OtherEvent:
		return []byte("otherEvent"), nil
	case CelebrationFestivity:
		return []byte("celebrationFestivity"), nil
	case Convention:
		return []byte("convention"), nil
	case ExamTest:
		return []byte("examTest"), nil
	case ServiceTime:
		return []byte("serviceTime"), nil
	case Exhibition:
		return []byte("exhibition"), nil
	case PracticalSemesterAccompanyingEvent:
		return []byte("practicalSemesterAccompanyingEvent"), nil
	case Training:
		return []byte("training"), nil
	case Tutorial2:
		return []byte("tutorial2"), nil
	case Workshop2:
		return []byte("workshop2"), nil
	case Tour:
		return []byte("tour"), nil
	case Speech:
		return []byte("speech"), nil
	case InformationSession:
		return []byte("informationSession"), nil
	case Course2:
		return []byte("course2"), nil
	case OnlineSeminar:
		return []byte("onlineSeminar"), nil
	case SeminarExcursion:
		return []byte("seminarExcursion"), nil
	case OnlineLecture:
		return []byte("onlineLecture"), nil
	case BasicCourse:
		return []byte("basicCourse"), nil
	case Sample:
		return []byte("sample"), nil
	case SummerSchool:
		return []byte("summerSchool"), nil
	case InauguralLecture:
		return []byte("inauguralLecture"), nil
	case Blockage:
		return []byte("blockage"), nil
	case Exercises:
		return []byte("exercise"), nil

	case Consultation:
		return []byte("consultation"), nil
	case RepeatExam:
		return []byte("repeatExam"), nil

	default:
		return nil, fmt.Errorf("unknown event type %d", e)
	}
}

func parseEventType(s string) (EventType, error) {
	switch s {
	case "Training course", "Schulung":
		return TrainingCourse, nil
	case "Proseminar":
		return Proseminar, nil
	case "Main seminar", "Hauptseminar":
		return MainSeminar, nil
	case "Exercise", "Übung":
		return Exercise, nil
	case "Colloquium", "Kolloquium":
		return Colloquium, nil
	case "Advanced seminar", "Oberseminar":
		return AdvancedSeminar, nil
	case "Project", "Projekt":
		return Project, nil
	case "Propaedeutic", "Propädeutikum":
		return Propaedeutic, nil
	case "Seminar/Exercise", "Seminar/Übung":
		return SeminarExercise, nil
	case "Field exercise", "Geländeübung":
		return FieldExercise, nil
	case "Tutorial", "Tutorium":
		return Tutorial, nil
	case "Excursion", "Exkursion":
		return Excursion, nil
	case "Training research project", "Lehrforschungsprojekt":
		return TrainingResearchProject, nil
	case "Optional lecture", "Wahlvorlesung":
		return OptionalLecture, nil
	case "Optional seminar", "Wahlseminar":
		return OptionalSeminar, nil
	case "Teleteaching":
		return Teleteaching, nil
	case "Consulting", "Beratung":
		return Consulting, nil
	case "Miscellaneous", "Sonstiges":
		return Miscellaneous, nil
	case "Working group", "Arbeitsgemeinschaft":
		return WorkingGroup, nil
	case "Revision course", "Repetitorium":
		return RevisionCourse, nil
	case "Workshop":
		return Workshop, nil
	case "Lecture/exercise", "Vorlesung/Übung":
		return LectureExercise, nil
	case "Lecture/seminar", "Vorlesung/Seminar":
		return LectureSeminar, nil
	case "Basic module", "Basismodul":
		return BasicModule, nil
	case "Practical work/seminar", "Praktikum/Seminar":
		return PracticalWorkSeminar, nil
	case "Lecture/practical work", "Vorlesung/Praktikum":
		return LecturePracticalWork, nil
	case "Practice module", "Praxismodul":
		return PracticeModule, nil
	case "Advanced module", "Aufbaumodul":
		return AdvancedModule, nil
	case "Language course", "Sprachkurs":
		return LanguageCourse, nil
	case "Practical work", "Praktikum":
		return PracticalWork, nil

	case "Seminar":
		return Seminar, nil
	case "Lecture", "Vorlesung":
		return Lecture, nil
	case "Block course", "Blockveranstaltung":
		return BlockCourse, nil

	case "Course", "Kurs":
		return Course, nil
	case "Cross-sectional area", "Querschnittsbereich":
		return CrosssectionalArea, nil
	case "Movie screening", "Filmvorführung":
		return MovieScreening, nil
	case "Exam",
		"Klausur",
		"Prüfung",
		"Klausurtermin",
		"Klausur Ersttermin",
		"Prüfung 1. Versuch":
		return Exam, nil
	case /* "Meeting", */ "Besprechung":
		return Conference, nil
	case "Conference/symposium", "Konferenz/Symposium":
		return ConferenceSymposium, nil
	case "Musical event", "Musikveranstaltung":
		return MusicalEvent, nil
	case "Meeting", "Treffen":
		return Meeting_, nil
	case "Talk", "Vortrag":
		return Talk, nil
	case "Lecture Series", "Ringvorlesung":
		return LectureSeries, nil
	case "Sitzung", "Conference":
		return Session, nil
	case "Other event", "Sonstige Veranstaltung":
		return OtherEvent, nil
	case "Celebration/festivity", "Feier/Festveranstaltung":
		return CelebrationFestivity, nil
	case "Convention", "Tagung":
		return Convention, nil
	case "Exam/test", "Klausur/Prüfung":
		return ExamTest, nil
	case "Service time", "Servicezeit":
		return ServiceTime, nil
	case "Exhibition", "Ausstellung":
		return Exhibition, nil
	case "Begleitveranstaltung zum Praxissemester":
		return PracticalSemesterAccompanyingEvent, nil
	case "Training", "Weiterbildung":
		return Training, nil
	// case "Tutorial", "Tutorium":
	//	return Tutorial2, nil
	// case "Workshop":
	//	return Workshop2, nil
	case "Tour", "Besichtigung":
		return Tour, nil
	case "Speech" /*, "Vortrag" */ :
		return Speech, nil
	case "Information session", "Informationsveranstaltung":
		return InformationSession, nil
	// case "Course", "Kurs":
	//	return Course2, nil
	case "Online seminar", "Online-Seminar":
		return OnlineSeminar, nil
	case "Seminar/Excursion", "Seminar/Exkursion":
		return SeminarExcursion, nil
	case "Online lecture", "Online-Vorlesung":
		return OnlineLecture, nil
	case "Basic course", "Grundkurs":
		return BasicCourse, nil
	case "Sample", "Probe":
		return Sample, nil
	case "Summer school", "Sommerschule":
		return SummerSchool, nil
	case "Inaugural lecture", "Antrittsvorlesung":
		return InauguralLecture, nil
	case "Blockage", "Blockierung":
		return Blockage, nil
	case "Exercises", "Übungen":
		return Exercises, nil

	case "Konsultation", "Fragestunde zur Klausur":
		return Consultation, nil
	case "Re-Exam",
		"Wiederholungsklausur",
		"Wiederholungsprüfung",
		"Nachklausur",
		"Nachprüfung",
		"Wdh-Klausur",
		"Zweitklausur",
		"Klausur Zweittermin",
		"2. Klausurtermin":
		return RepeatExam, nil
	default:
		return 0, fmt.Errorf("unknown event type: '%s'", s)
	}
}
