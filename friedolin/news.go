package friedolin

import (
	"strings"

	"lelux.net/taro"
	"lelux.net/x/html"
)

type News struct {
	Date  taro.Date `json:"date"`
	Title string    `json:"title"`
	Body  *HTML     `json:"body"`
}

func (f *Fridolin) News() ([]News, error) {
	n, err := f.user(false, 0, nil)
	if err != nil && err != ErrLoggedOut {
		return nil, err
	}

	var news []News

	box := n.Query(newsbox).NextSibling.NextSibling

	l := trTable((*html.Node)(box))
	for {
		firstCol := l.FirstChild.NextSibling
		dateStr := strings.TrimSuffix(firstCol.FirstChild.FirstChild.Data, "\u00a0")
		date, err := parseDate(dateStr)
		if err != nil {
			return news, err
		}
		n := News{Date: date}

		title := firstCol.NextSibling.NextSibling.FirstChild
		n.Title = title.FirstChild.Data

		body := title.NextSibling.NextSibling
		body.PrevSibling = nil
		n.Body = CleanHtml(body, baseUrl)

		news = append(news, n)

		if !l.Next() {
			break
		}
	}

	return news, nil
}
