package friedolin

import (
	"errors"
	"fmt"
	"log"
	"net/url"
	"strconv"
	"strings"

	"golang.org/x/net/html/atom"
	"lelux.net/taro"
	"lelux.net/x/html"
)

func nextStyleTable(n *html.Node) (*html.Node, string) {
	for {
		n = (*html.Node)(n.NextSibling)
		if n == nil {
			return nil, ""
		}

		if n.DataAtom != atom.Table {
			continue
		}

		style := n.GetAttr("style")
		if style != "" {
			return n, style
		}
	}
}

type GradeSubject struct {
	Modules []GradeModule `json:"modules"`
}

type GradeModule struct {
	ShortText string `json:"shortText"`
	Name      string `json:"name"`
	Term      Term   `json:"term"`

	Grade   float32 `json:"grade,omitempty"`
	Credits uint8   `json:"credits"`

	Exams []GradeExam `json:"exams"`
}

type GradeExam struct {
	Number uint32 `json:"number"`
	Term   Term   `json:"term"`

	Grade  float32     `json:"grade,omitempty"`
	Reason GradeReason `json:"reason"`
	Try    uint8       `json:"try"`

	ExamDate         *taro.Date `json:"examDate"`
	Examiner         string     `json:"examiner"`
	AnnouncementDate taro.Date  `json:"announcementDate"`

	Units []GradeUnit `json:"units"`
}

type GradeReason uint8

const (
	_ GradeReason = iota
	NotAdmitted
	Ungraded
	Cheating
	Absent
	Withdrawal
	WithdrawalMedical
)

func (r GradeReason) String() string {
	switch r {
	case NotAdmitted:
		return "NotAdmitted"
	case Ungraded:
		return "Ungraded"
	case Cheating:
		return "Cheating"
	case Absent:
		return "Absent"
	case Withdrawal:
		return "Withdrawal"
	case WithdrawalMedical:
		return "WithdrawalMedical"
	default:
		return fmt.Sprintf("%%!GradeReason(%d)", r)
	}
}

func (r GradeReason) MarshalText() ([]byte, error) {
	switch r {
	case NotAdmitted:
		return []byte("notAdmitted"), nil
	case Ungraded:
		return []byte("ungraded"), nil
	case Cheating:
		return []byte("cheating"), nil
	case Absent:
		return []byte("absent"), nil
	case Withdrawal:
		return []byte("withdrawal"), nil
	case WithdrawalMedical:
		return []byte("withdrawalMedical"), nil
	default:
		return nil, fmt.Errorf("unknown grade reason %d", r)
	}
}

func (r *GradeReason) UnmarshalText(b []byte) error {
	switch string(b) {
	case "notAdmitted":
		*r = NotAdmitted
	case "ungraded":
		*r = Ungraded
	case "cheating":
		*r = Cheating
	case "absent":
		*r = Absent
	case "withdrawal":
		*r = Withdrawal
	case "withdrawalMedical":
		*r = WithdrawalMedical
	default:
		return fmt.Errorf("unknown grade reason '%s'", b)
	}
	return nil
}

type GradeUnit struct {
	Number uint32 `json:"number"`
	Name   string `json:"name"`

	Events []GradeEvent `json:"events"`
}

type GradeEvent struct {
	Type   EventType `json:"type"`
	Number uint32    `json:"number"`
	Name   string    `json:"name"`

	Term Term `json:"term"`
}

func (f *Fridolin) Grades(degree uint8) ([]GradeSubject, error) {
	err := f.fetchAsi()
	if err != nil {
		return nil, err
	}

	n, err := f.htmlReq(false,
		"notenspiegelStudent",
		url.Values{
			"next":        {"list.vm"},
			"createInfos": {"Y"},
			"struct":      {"auswahlBaum"},
			"nodeID":      {fmt.Sprintf("auswahlBaum|abschluss:abschl=%d,stgnr=1", degree)},
			"asi":         {f.Asi},
		})
	if err != nil {
		return nil, err
	}

	entry := (*html.Node)(n.Query(form).FirstChild)
	entry, _ = nextStyleTable(entry)

	var s GradeSubject

	for entry != nil {
		var exams []GradeExam

		examEntry, style := nextStyleTable(entry)
		for {
			if examEntry == nil {
				break
			}

			moduleHeading := strings.HasSuffix(style, "0.0em;")
			if !moduleHeading {
				break
			}

			var e GradeExam

			examTable := list{examEntry.Query(td)}

			if examTable.GetAttr("class") != "" {
				break
			}

			numberStr := examTable.FirstChild.Data
			_, numberPart, ok := strings.Cut(strings.TrimSpace(numberStr), " ")
			if !ok {
				return nil, errors.New("missing space before exam number")
			}
			number, err := strconv.ParseUint(numberPart, 10, 32)
			if err != nil {
				return nil, fmt.Errorf("failed to parse exam number: %w", err)
			}
			e.Number = uint32(number)
			examTable.Next()

			examTable.Next()

			e.Term, err = ParseTerm(strings.TrimSpace(examTable.FirstChild.Data))
			if err != nil {
				return nil, fmt.Errorf("failed to parse exam term: %w", err)
			}
			examTable.Next()

			gradeStr := strings.TrimSpace(examTable.FirstChild.Data)
			if gradeStr != "" {
				e.Grade, err = parseCommaFloat(gradeStr)
				if err != nil {
					return nil, fmt.Errorf("failed to parse exam grade: %w", err)
				}
			}
			examTable.Next()

			examTable.Next()

			examTable.Next()

			reasonField := (*html.Node)(examTable.FirstChild.NextSibling)
			reason := strings.TrimSpace(reasonField.FirstChild.Data)
			switch reason {
			case "":
			case "NZU":
				e.Reason = NotAdmitted
			case "++":
				e.Reason = Ungraded
			case "NE":
				e.Reason = Absent
			case "TA":
				e.Reason = Cheating
			case "RAN":
				e.Reason = Withdrawal
			case "RAT":
				e.Reason = WithdrawalMedical
			default:
				log.Printf("unknown grade reason '%s' with title '%s'", reason, reasonField.GetAttr("title"))
			}
			examTable.Next()

			examTable.Next()

			try, err := strconv.ParseUint(strings.TrimSpace(examTable.FirstChild.Data), 10, 32)
			if err != nil {
				return nil, err
			}
			e.Try = uint8(try)
			examTable.Next()

			examDateStr := strings.TrimSpace(examTable.FirstChild.Data)
			if examDateStr != "" {
				date, err := parseDate(examDateStr)
				if err != nil {
					return nil, err
				}
				e.ExamDate = &date
			}
			examTable.Next()

			e.Examiner = strings.TrimSpace(examTable.FirstChild.Data)
			examTable.Next()

			e.AnnouncementDate, err = parseDate(strings.TrimSpace(examTable.FirstChild.Data))
			if err != nil {
				return nil, err
			}

			for {
				examEntry, style = nextStyleTable(examEntry)
				if examEntry == nil || examEntry.GetAttr("cellpadding") == "2" {
					break
				}

				entryTable := trTable(examEntry)

				heading := strings.TrimSpace(entryTable.FirstChild.NextSibling.FirstChild.Data)
				_, heading, ok := strings.Cut(heading, ": ")
				if !ok {
					return nil, fmt.Errorf("missing ': ' in unit heading")
				}

				numberStr, name, ok := strings.Cut(heading, " ")
				if !ok {
					return nil, fmt.Errorf("missing space after unit number")
				}
				number, err := strconv.ParseUint(numberStr, 10, 32)
				if err != nil {
					return nil, err
				}

				u := GradeUnit{Number: uint32(number), Name: name}

				for entryTable.Next() {
					numberField := entryTable.Query(strong)
					number, err := strconv.ParseUint(numberField.FirstChild.Data, 10, 32)
					if err != nil {
						return nil, fmt.Errorf("failed to parse event number: %w", err)
					}
					e := GradeEvent{Number: uint32(number)}

					textField := numberField.Parent.NextSibling.NextSibling
					nameField := textField.FirstChild.NextSibling
					e.Name = nameField.FirstChild.Data

					metaStr, ok := strings.CutPrefix(nameField.NextSibling.Data, " - ")
					if !ok {
						return nil, fmt.Errorf("missing ' - ' before event meta")
					}
					typeStr, termStr, ok := strings.Cut(metaStr, " - ")
					if !ok {
						return nil, fmt.Errorf("missing ' - ' between event type and term")
					}

					e.Type, err = parseEventType(typeStr)
					if err != nil {
						return nil, err
					}
					e.Term, err = ParseTerm(strings.TrimSpace(termStr))
					if err != nil {
						return nil, err
					}

					u.Events = append(u.Events, e)
				}

				e.Units = append(e.Units, u)
			}

			exams = append(exams, e)
		}

		if len(exams) > 0 {
			m := GradeModule{Exams: exams}

			moduleTable := list{entry.Query(td)}

			m.ShortText = moduleTable.FirstChild.NextSibling.FirstChild.Data
			moduleTable.Next()

			m.Name = strings.TrimSpace(moduleTable.FirstChild.Data)
			moduleTable.Next()

			m.Term, err = ParseTerm(strings.TrimSpace(moduleTable.FirstChild.Data))
			if err != nil {
				return nil, fmt.Errorf("failed to parse module term: %w", err)
			}
			moduleTable.Next()

			gradeStr := strings.TrimSpace(moduleTable.FirstChild.Data)
			if gradeStr != "" {
				m.Grade, err = parseCommaFloat(gradeStr)
				if err != nil {
					return nil, fmt.Errorf("failed to parse module grade: %w", err)
				}
			}
			moduleTable.Next()

			moduleTable.Next()

			credits, err := strconv.ParseUint(strings.TrimSpace(moduleTable.FirstChild.Data), 10, 8)
			if err != nil {
				return nil, fmt.Errorf("failed to parse credits: %w", err)
			}
			m.Credits = uint8(credits)

			s.Modules = append(s.Modules, m)
		}

		entry = examEntry
	}

	return []GradeSubject{s}, nil
}
