package thueringen

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"unicode"

	ghtml "golang.org/x/net/html"
	"lelux.net/taro"
	"lelux.net/taro/cafeteria"
	"lelux.net/x/html"
)

type Thüringen struct {
	Id uint8
}

var (
	groupWrapper    = html.MustCompile(".splGroupWrapper")
	pl3             = html.MustCompile(".pl-3")
	mealText        = html.MustCompile(".mealText")
	mealPreise      = html.MustCompile(".mealPreise")
	kennzKommaliste = html.MustCompile(".kennzKommaliste")
	komponenten     = html.MustCompile(".komponenten")
)

var (
	Jena_ErstAbbePlatz       = Thüringen{41}
	Jena_CarlZeissPromenade  = Thüringen{58}
	Jena_Philosophenweg      = Thüringen{59}
	Jena_UniHauptgebäude     = Thüringen{64}
	Jena_MoritzVonRohrStraße = Thüringen{61}
	Jena_ZurRosen            = Thüringen{63}

	Erfurt_NordhäuserStraße = Thüringen{44}
	Erfurt_AltonaerStraße   = Thüringen{47}
	Erfurt_Schlüterstraße   = Thüringen{51}
	Erfurt_LeipzigerStraße  = Thüringen{52}

	Ilmenau_Ehrenberg          = Thüringen{46}
	Ilmenau_CafeteriaEhrenberg = Thüringen{53}
	Ilmenau_Nanoteria          = Thüringen{55}
	Ilmenau_Röntgenbau         = Thüringen{57}

	Gera_WegDerFreundschaft = Thüringen{67}

	Eisenach_AmWartenberg = Thüringen{69}

	Nordhausen_Weinberghof = Thüringen{71}

	Schmalkalden_Blechhammer = Thüringen{73}

	Weimar_AmPark          = Thüringen{77}
	Weimar_AmHorn          = Thüringen{76}
	Weimar_Schwanseestraße = Thüringen{80}
)

func (t Thüringen) Meals(ctx context.Context, date taro.Date) ([]cafeteria.Meal, error) {
	body := fmt.Sprintf("resources_id=%d&date=%s", t.Id, date.Format("02.01.2006"))
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, "https://www.stw-thueringen.de/xhr/loadspeiseplan.html", strings.NewReader(body))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	n, err := html.Parse(res.Body)
	if err != nil {
		return nil, err
	}

	groups := n.QueryAll(groupWrapper)
	meals := make([]cafeteria.Meal, len(groups))
	for i, group := range groups {
		meal := &meals[i]

		heading := (*html.Node)(group.FirstChild.NextSibling)

		timeNode := heading.Query(pl3).FirstChild
		if timeNode != nil {
			timeStr := timeNode.Data
			switch timeStr {
			case "Mittagessen":
				meal.Time = cafeteria.Noon
			case "Zwischenversorgung":
				meal.Time = cafeteria.Afternoon
			case "Abendessen":
				meal.Time = cafeteria.Evening
			default:
				return nil, fmt.Errorf("unknown meal time: '%s'", timeStr)
			}
		}

		for c := heading; c != nil; c = (*html.Node)(c.NextSibling) {
			text := c.Query(mealText)
			if text == nil {
				continue
			}

			var dish cafeteria.Dish

			title := text.FirstChild.Data
			i := strings.IndexFunc(title, func(r rune) bool {
				return unicode.ToUpper(r) != r
			})
			if i > 5 {
				i = strings.LastIndexByte(title[:i], ' ')
			}
			if i > 5 {
				dish.Food.Description = title[i:]
				title = strings.TrimSpace(title[:i])
			}
			dish.Food.Name = title

			allergens := strings.Split(c.Query(kennzKommaliste).FirstChild.Data, ",")
			for _, allergen := range allergens {
				dish.Food.Diet |= parseAllergen(allergen)
			}

			priceS := strings.TrimSuffix(strings.TrimSpace(c.Query(mealPreise).LastChild.Data), " €")
			prices := strings.SplitN(priceS, " / ", 3)
			dish.StudentPrice, _ = parsePrice(prices[0])
			dish.EmployeePrice, _ = parsePrice(prices[1])
			dish.GuestPrice, _ = parsePrice(prices[2])

			components := c.Query(komponenten)
			if components != nil {
				for c := components.FirstChild.FirstChild; c != nil; c = c.NextSibling {
					if c.Type != ghtml.TextNode {
						continue
					}

					title := strings.TrimPrefix(c.Data, "• ")
					ingredient := cafeteria.Food{Name: title}

					i := strings.LastIndexByte(title, '(')
					if i != -1 {
						allergens := strings.Split(title[i+1:len(title)-1], ",")
						for _, allergen := range allergens {
							ingredient.Diet |= parseAllergen(allergen)
						}
						title = strings.TrimSpace(title[:i])
					}
					ingredient.Name = strings.TrimSuffix(title, " (reg.)")
					if len(ingredient.Name) != len(title) {
						ingredient.Diet |= cafeteria.Regional
					}

					dish.Ingredients = append(dish.Ingredients, ingredient)
				}
			}

			meal.Dishes = append(meal.Dishes, dish)
		}
	}
	return meals, nil
}

func parseAllergen(s string) cafeteria.Diet {
	switch s {
	case "V":
		return cafeteria.Vegetarian
	case "V*":
		return cafeteria.Vegan
	case "mr":
		return cafeteria.Regional
	case "BIO":
		return cafeteria.Organic

	case "1":
		return cafeteria.Coloring
	case "2":
		return cafeteria.Preservative
	case "3":
		return cafeteria.Antioxidant
	case "4":
		return cafeteria.FlavorEnhancer
	case "5":
		return cafeteria.Sulphurized
	case "6":
		return cafeteria.Blackened
	case "7":
		return cafeteria.Waxed
	case "8":
		return cafeteria.Phosphate
	case "9":
		return cafeteria.Sweetener
	case "10":
		return cafeteria.Phenylalanine
	case "14":
		return cafeteria.CocoabasedFatGlaze
	case "15":
		return cafeteria.Phenylalanine
	case "16":
		return cafeteria.Quinine

	case "T1":
		return cafeteria.AnimalGelatin
	case "T2":
		return cafeteria.AnimalRennet
	case "T3":
		return cafeteria.Carmine
	case "T4":
		return cafeteria.Sepia
	case "T5":
		return cafeteria.Honey

	case "Wz":
		return cafeteria.Wheat
	case "Ro":
		return cafeteria.Rye
	case "Gs":
		return cafeteria.Barley
	case "Hf":
		return cafeteria.Oats
	case "Di":
		return cafeteria.Spelt
	case "Ka":
		return cafeteria.KhorasanWheat
	case "Kr":
		return cafeteria.Crustacean
	case "Ei":
		return cafeteria.ChickenEgg
	case "Fi":
		return cafeteria.Fish
	case "Er":
		return cafeteria.Peanut
	case "So":
		return cafeteria.Soy
	case "Mi":
		return cafeteria.Milk
	case "Ma":
		return cafeteria.Almond
	case "Ha":
		return cafeteria.Hazel
	case "Wa":
		return cafeteria.Walnut
	case "Ca":
		return cafeteria.Cashew
	case "Pe":
		return cafeteria.Pekan
	case "Pa":
		return cafeteria.BrazilNut
	case "Pi":
		return cafeteria.Pistachio
	case "Mc":
		return cafeteria.Macadamia
	case "Sel":
		return cafeteria.Celery
	case "Sen":
		return cafeteria.Mustard
	case "Ses":
		return cafeteria.Sesame
	case "Su":
		return cafeteria.Sulfur
	case "Lu":
		return cafeteria.Lupine
	case "We":
		return cafeteria.Mollusk

	case "A":
		return cafeteria.Alcohol
	case "F":
		return cafeteria.Fish
	case "G":
		return cafeteria.Poultry
	case "K":
		return cafeteria.Garlic
	case "Kn":
		return cafeteria.Rabbit
	case "L":
		return cafeteria.Lamb
	case "R":
		return cafeteria.Beef
	case "S":
		return cafeteria.Pork
	case "W":
		return cafeteria.GameMeat
	case "Ws":
		return cafeteria.WildBoar

	case "odZ", "leerTheke":
		return 0
	default:
		log.Printf("unknown allergen: '%s'\n", s)
		return 0
	}
}

func parsePrice(s string) (cafeteria.Price, error) {
	v, err := strconv.ParseFloat(strings.Replace(s, ",", ".", 1), 32)
	if err != nil {
		return cafeteria.Price{}, err
	}
	return cafeteria.Price{Value: float32(v), Currency: "EUR"}, nil
}
