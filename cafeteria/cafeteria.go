package cafeteria

import (
	"context"
	"fmt"

	"lelux.net/taro"
)

type Cafeteria interface {
	Meals(ctx context.Context, date taro.Date) ([]Meal, error)
}

type Meal struct {
	Time   MealTime `json:"time,omitempty"`
	Dishes []Dish   `json:"dishes"`
}

type MealTime uint8

const (
	_ MealTime = iota
	Morning
	Noon
	Afternoon
	Evening
)

func (m MealTime) MarshalText() ([]byte, error) {
	switch m {
	case Morning:
		return []byte("morning"), nil
	case Noon:
		return []byte("noon"), nil
	case Afternoon:
		return []byte("afternoon"), nil
	case Evening:
		return []byte("evening"), nil
	default:
		return nil, fmt.Errorf("unknown meal time %d", m)
	}
}

type Dish struct {
	Food

	StudentPrice  Price `json:"studentPrice"`
	EmployeePrice Price `json:"employeePrice"`
	GuestPrice    Price `json:"guestPrice"`

	Ingredients []Food `json:"ingredients"`
}

type Price struct {
	Value    float32 `json:"value"`
	Currency string  `json:"currency"`
}

type Food struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Diet        Diet   `json:"diet"`
}

type Diet uint64

const (
	Vegetarian Diet = 1 << iota
	Vegan
	Regional
	Organic

	Coloring
	Preservative
	Antioxidant
	FlavorEnhancer
	Sulphurized
	Blackened
	Waxed
	Phosphate
	Sweetener
	Phenylalanine
	CocoabasedFatGlaze
	Caffeine
	Quinine
	AnimalGelatin
	AnimalRennet
	Carmine
	Sepia
	Honey

	Wheat
	Rye
	Barley
	Oats
	Spelt
	KhorasanWheat
	Crustacean
	ChickenEgg
	Fish
	Peanut
	Soy
	Milk
	Almond
	Hazel
	Walnut
	Cashew
	Pekan
	BrazilNut
	Pistachio
	Macadamia
	Celery
	Mustard
	Sesame
	Sulfur
	Lupine
	Mollusk

	Alcohol
	Poultry
	Garlic
	Rabbit
	Lamb
	Beef
	Pork
	GameMeat
	WildBoar
)

const (
	Gluten = Wheat | Rye | Barley | Oats | Spelt | KhorasanWheat
)
