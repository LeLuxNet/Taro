package main

import (
	"bytes"
	"context"
	"embed"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"
	"unicode"
	"unicode/utf8"

	"lelux.net/taro"
	"lelux.net/taro/cafeteria"
	"lelux.net/taro/friedolin"
	"lelux.net/taro/meinjena"
	"lelux.net/taro/moodle"
)

var sessionCookie = "taro_session"

//go:embed *.html
var templsFS embed.FS

const (
	layoutHtml = "layout.html"
)

var funcs = template.FuncMap{
	"formatEventType": func(e friedolin.EventType) string {
		switch e {
		case friedolin.Exercise:
			return "Übung"
		case friedolin.Lecture:
			return "Vorlesung"
		case friedolin.Tutorial:
			return "Tutorium"
		case friedolin.LanguageCourse:
			return "Sprachkurs"
		case friedolin.LectureExercise:
			return "Vorlesung/Übung"
		case friedolin.PracticalWork:
			return "Praktikum"
		case friedolin.Exam:
			return "Prüfung"
		case friedolin.Consultation:
			return "Fragen zur Prüfung"
		case friedolin.RepeatExam:
			return "Nachprüfung"
		default:
			return fmt.Sprintf("<%d>", e)
		}
	},
	"formatDate": func(d taro.Date) string {
		return fmt.Sprintf("%d. %s", d.Day, formatMonth(d.Month))
	},
	"formatRelativeDate": func(prefix string, d taro.Date) string {
		today := taro.Today()
		if d == today.AddDate(0, 0, -1) {
			return "gestern"
		} else if d == today {
			return "heute"
		} else if d == today.AddDate(0, 0, 1) {
			return "morgen"
		} else if d == today.AddDate(0, 0, 2) {
			return "übermorgen"
		} else {
			return fmt.Sprintf("%s%d. %s", prefix, d.Day, formatMonth(d.Month))
		}
	},
	"formatGrade": func(n float32) string {
		nFloor := float64(int(n*10)) / 10
		s := strconv.FormatFloat(nFloor, 'f', 1, 32)
		return strings.Replace(s, ".", ",", 1)
	},
	"formatMoney": func(m cafeteria.Price) string {
		s := strconv.FormatFloat(float64(m.Value), 'f', 2, 32)
		s = strings.Replace(s, ".", ",", 1)
		switch m.Currency {
		case "EUR":
			return s + " €"
		default:
			return s + " " + m.Currency
		}
	},
	"dietInfo": func(d cafeteria.Diet) []DietItem {
		return dietInfo(d)
	},
	"dietColor": func(d cafeteria.Diet) template.CSS {
		if d&cafeteria.Vegan != 0 {
			if d&cafeteria.Gluten == 0 {
				return "#3b82f6" // blue
			} else {
				return "#22c55e" // green
			}
		} else if d&cafeteria.Vegetarian != 0 {
			return "#f59e0b" // orange
		} else {
			return "#ef4444" // red
		}
	},

	"hashColor": hashColor,
	"html": func(h *friedolin.HTML) (template.HTML, error) {
		s, err := h.Render()
		return template.HTML(s), err
	},

	"monthInt": func(m time.Month) uint8 {
		return uint8(m)
	},
	"padZero": func(pad int, n uint8) string {
		s := strconv.FormatUint(uint64(n), 10)
		return strings.Repeat("0", pad-len(s)) + s
	},
	"dateOver": func(d taro.Date) bool {
		return d.Before(taro.Today())
	},
}

func formatMonth(m time.Month) string {
	switch m {
	case time.January:
		return "Januar"
	case time.February:
		return "Februar"
	case time.March:
		return "März"
	case time.April:
		return "April"
	case time.May:
		return "Mai"
	case time.June:
		return "Juni"
	case time.July:
		return "Juli"
	case time.August:
		return "August"
	case time.September:
		return "September"
	case time.October:
		return "Oktober"
	case time.November:
		return "November"
	case time.December:
		return "Dezember"
	default:
		return m.String()
	}
}

func hashColor(l float64, chroma float64, hash float64) template.CSS {
	return template.CSS(fmt.Sprintf("oklch(%f %f %f)", l, chroma, -hash*360-55))
}

type Render struct {
	Domain  string
	Meal    cafeteria.MealTime
	Session *Session
	Data    interface{}
}

type Error struct {
	Title string
	Api   ApiError
	Code  int
}

func wrapErr(err error, part, path string, ids []uint) Error {
	switch err {
	case friedolin.ErrLoggedOut:
		return Error{
			"Abgemeldet",
			ApiError{"loggedOut", ""},
			http.StatusUnauthorized,
		}
	case friedolin.ErrFunctionalityUnavailable:
		return Error{
			"Funktionalität im Moment nicht verfügbar",
			ApiError{"functionalityUnavailable", ""},
			http.StatusServiceUnavailable,
		}
	case friedolin.ErrWrongCredentials:
		return Error{
			"URZ-Kürzel oder Passwort falsch",
			ApiError{"wrongCredentials", ""},
			http.StatusUnauthorized,
		}
	case friedolin.ErrUnavailable:
		return Error{
			"Friedolin nicht erreichbar",
			ApiError{"unavailable", ""},
			http.StatusBadGateway,
		}
	case friedolin.ErrNotExist:
		return Error{
			"Nicht gefunden",
			ApiError{"notExist", ""},
			http.StatusNotFound,
		}
	default:
		if err2, ok := err.(friedolin.UserError); ok {
			return Error{
				err2.Message,
				ApiError{"userError", err2.Message},
				http.StatusBadRequest,
			}
		} else if err2, ok := err.(meinjena.Error); ok {
			return Error{
				err2.Message,
				ApiError{"meinjenaError", err2.Message},
				http.StatusBadRequest,
			}
		} else {
			log.Printf("error handling %s %s/%v: %s", part, path, ids, err)
			return Error{
				"Interner Fehler",
				ApiError{"internalError", ""},
				http.StatusInternalServerError,
			}
		}
	}
}

type Session struct {
	Fridolin friedolin.Fridolin
	Moodle   moodle.Moodle
	LoggedIn bool
	Info     friedolin.MyInfo
	Setting  *Setting
}

var sessions sync.Map

func (s *Session) Check() error {
	info, err := s.Fridolin.MyInfo()
	if err == nil {
		s.Info = info

		settingsLock.Lock()
		var ok bool
		s.Setting, ok = settings[s.Info.Username]
		if !ok {
			s.Setting = &Setting{}
			settings[s.Info.Username] = s.Setting
		}
		SaveSettings()
	}
	return err
}

func (s *Session) bootstrap() bool {
	err := s.Check()
	if err == friedolin.ErrLoggedOut {
		return false
	} else if err != nil {
		log.Println("failed to fetch user info:", err)
	}
	return true
}

func loadTempl(path string) *template.Template {
	if path == "" {
		path = "index"
	}

	b, err := templsFS.ReadFile(path + ".html")
	if err != nil {
		panic(err)
	}

	pageTmpl := fmt.Sprintf("{{template \"%s\" .}}\n%s", layoutHtml, b)
	layoutTempl := template.Must(template.New(layoutHtml).Funcs(funcs).ParseFS(templsFS, layoutHtml, "schedule_cmp.html"))
	return template.Must(layoutTempl.New(path).Parse(pageTmpl))
}

var errTempl = loadTempl("error")

func Route(path string, idsCount int, post func(s *Session, ids []uint, get, post url.Values) (string, interface{}, error), get func(ctx context.Context, s *Session, ids []uint) (interface{}, error)) {
	fullPath := "/" + path
	templ := loadTempl(path)

	handle := func(w http.ResponseWriter, r *http.Request) {
		idParts := strings.Split(strings.TrimPrefix(r.URL.Path, fullPath), "/")[1:]

		ids := make([]uint, 0, len(idParts))
		for _, part := range idParts {
			var err error
			id64, err := strconv.ParseUint(part, 10, 32)
			if err != nil {
				break
			}
			ids = append(ids, uint(id64))
		}

		if len(ids) < idsCount {
			http.Error(w, "not enough ids", http.StatusBadRequest)
			return
		}

		domain := r.Host
		i := strings.IndexAny(r.Host, ":.")
		if i != -1 {
			domain = domain[:i]
		}

		var domainName strings.Builder
		for _, r := range domain {
			l := utf8.RuneLen(r)
			domain = domain[l:]

			tr := unicode.ToTitle(r)
			tl := utf8.RuneLen(tr)

			domainName.Grow(tl + len(domain))
			domainName.WriteRune(tr)
			domainName.WriteString(domain)
			break
		}
		domain = domainName.String()

		var s *Session
		cookie, cErr := r.Cookie(sessionCookie)
		if cErr == nil && cookie.Value != "" {
			sAny, loaded := sessions.LoadOrStore(cookie.Value, &Session{
				Fridolin: friedolin.Fridolin{Session: cookie.Value},
				LoggedIn: true})
			s = sAny.(*Session)
			if !loaded && !s.bootstrap() {
				*s = Session{}
				s = &Session{}
			}
		} else {
			s = &Session{}
		}

		var data interface{}
		switch r.Method {
		case http.MethodGet:
			if get != nil {
				var err error
				data, err = get(r.Context(), s, ids)
				switch err {
				case nil:
				case friedolin.ErrLoggedOut:
					http.Redirect(w, r, "/login?redirect="+url.QueryEscape(r.URL.RequestURI()), http.StatusTemporaryRedirect)
					return
				default:
					data = wrapErr(err, "get", path, ids)
				}
			}
		case http.MethodPost:
			if post != nil {
				err := r.ParseForm()
				if err != nil {
					http.Error(w, "failed to parse form", http.StatusBadRequest)
					return
				}

				var url string
				url, data, err = post(s, ids, r.URL.Query(), r.PostForm)
				if err != nil {
					data = wrapErr(err, "post", path, ids)
					break
				}
				if s.Fridolin.Session != "" {
					sessions.Store(s.Fridolin.Session, s)
				}

				http.SetCookie(w, &http.Cookie{
					Name:     sessionCookie,
					Value:    s.Fridolin.Session,
					Path:     "/",
					Secure:   true,
					HttpOnly: true,
					MaxAge:   24 * 60 * 60,
				})

				if url != "" {
					http.Redirect(w, r, url, http.StatusSeeOther)
					return
				} else {
					break
				}
			}
			fallthrough
		default:
			http.Error(w, "unsupported method", http.StatusMethodNotAllowed)
		}

		if r, ok := data.(io.Reader); ok {
			io.Copy(w, r)
			if rc, ok := r.(io.Closer); ok {
				rc.Close()
			}
			return
		}

		status := http.StatusOK
		var t *template.Template
		if wErr, ok := data.(Error); ok {
			t = errTempl
			status = wErr.Code
		} else {
			t = templ
		}

		now := time.Now()
		tz, err := time.LoadLocation("Europe/Berlin")
		if err == nil {
			now = now.In(tz)
		}
		hour := now.Hour()
		var mealTime cafeteria.MealTime
		switch {
		case hour < 10:
			mealTime = cafeteria.Morning
		case hour < 14:
			mealTime = cafeteria.Noon
		case hour < 17:
			mealTime = cafeteria.Afternoon
		default:
			mealTime = cafeteria.Evening
		}

		var b bytes.Buffer
		err = t.Execute(&b, Render{domain, mealTime, s, data})
		if err != nil {
			if t == errTempl {
				log.Printf("error executing error template %s/%v: %s", path, ids, err)
			} else {
				log.Printf("error executing template %s/%v: %s", path, ids, err)
			}
			http.Error(w, "failed to render page", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		w.Header().Set("Content-Length", strconv.Itoa(b.Len()))
		w.WriteHeader(status)
		io.Copy(w, &b)
	}

	http.HandleFunc(fullPath, handle)
	http.HandleFunc(fullPath+"/", handle)
}

type ApiError struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

func ApiRoute(postOnly bool, path string, withId bool, handle func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error)) {
	fullPath := "/api/" + path
	if withId {
		fullPath += "/"
	}

	http.HandleFunc(fullPath, func(w http.ResponseWriter, r *http.Request) {
		var id uint
		if withId {
			var err error
			id64, err := strconv.ParseUint(strings.TrimPrefix(r.URL.Path, fullPath), 10, 32)
			if err != nil {
				http.Error(w, "missing id suffix", http.StatusBadRequest)
				return
			}
			id = uint(id64)
		}

		if postOnly && r.Method != http.MethodPost {
			http.Error(w, "only post requests supported", http.StatusMethodNotAllowed)
			return
		}

		w.Header().Set("Content-Type", "application/json; charset=utf-8")

		q := r.URL.Query()

		var f friedolin.Fridolin
		session, ok := strings.CutPrefix(r.Header.Get("Authorization"), "Bearer ")
		if ok {
			f.Session = session
		} else {
			f.Session = q.Get("session")
		}
		f.Language = q.Get("language")

		data, err := handle(f, id, q)
		if err != nil {
			err2 := wrapErr(err, "api", path, []uint{id})
			w.WriteHeader(err2.Code)
			data = err2.Api
		}

		e := json.NewEncoder(w)
		e.SetEscapeHTML(false)
		err = e.Encode(data)
		if err != nil {
			log.Printf("failed to encode json: %s", err)
			http.Error(w, "internal error", http.StatusInternalServerError)
		}
	})
}
