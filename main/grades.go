package main

import (
	"encoding/csv"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"lelux.net/taro/friedolin"
	"lelux.net/taro/moodle"
)

var gradesDir = "grades"

type LocalGrades struct {
	MatNumberFriedolin map[uint32]map[string][]Grade
	MoodleMap          map[moodle.AssignId]GradeMap
}

type GradeMap []GradeMapEntry

type GradeMapEntry struct {
	Points float32
	Grade  float32
}

func (g GradeMap) Resolve(p float32) float32 {
	for _, entry := range g {
		if entry.Points <= p {
			return entry.Grade
		}
	}
	return 0
}

type Grade struct {
	Name   string
	Parts  []float32
	Sum    float32
	Reason friedolin.GradeReason
}

func (g *LocalGrades) Load() error {
	entries, err := os.ReadDir(gradesDir)
	if err != nil {
		return err
	}

	for _, entry := range entries {
		if entry.IsDir() {
			dirs, err := os.ReadDir(filepath.Join(gradesDir, entry.Name()))
			if err != nil {
				return err
			}

			for _, d := range dirs {
				err = g.AddModuleGrades(entry.Name(), d.Name())
				if err != nil {
					return err
				}
			}
		} else {
			g.AddMoodleMap(entry.Name())
		}
	}

	return nil
}

func (g *LocalGrades) AddModuleGrades(module, name string) error {
	f, err := os.Open(filepath.Join(gradesDir, module, name))
	if err != nil {
		return err
	}
	defer f.Close()

	name = strings.TrimSuffix(name, ".csv")

	r := csv.NewReader(f)
	_, err = r.Read()
	if err != nil {
		return err
	}

	for {
		rec, err := r.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}

		matNumber, err := strconv.ParseUint(rec[0], 10, 32)
		if err != nil {
			return err
		}

		partsStr := rec[1 : len(rec)-1]
		grade := Grade{Name: name}

		mainStr := rec[len(rec)-1]
		sum, err := strconv.ParseFloat(mainStr, 32)
		if err == nil {
			grade.Sum = float32(sum)

			grade.Parts = make([]float32, len(partsStr))
			for i, s := range partsStr {
				if s == "" {
					continue
				}

				n, err := strconv.ParseFloat(s, 32)
				if err != nil {
					return err
				}
				grade.Parts[i] = float32(n)
			}
		} else {
			err = grade.Reason.UnmarshalText([]byte(mainStr))
			if err != nil {
				return err
			}
		}

		matModule := g.MatNumberFriedolin[uint32(matNumber)]
		if matModule == nil {
			matModule = make(map[string][]Grade)
			g.MatNumberFriedolin[uint32(matNumber)] = matModule
		}
		matModule[module] = append(matModule[module], grade)
	}

	return nil
}

func (g *LocalGrades) AddMoodleMap(name string) error {
	f, err := os.Open(filepath.Join(gradesDir, name))
	if err != nil {
		return err
	}
	defer f.Close()

	name = strings.TrimSuffix(name, ".map.csv")
	id, err := strconv.ParseUint(name, 10, 32)
	if err != nil {
		return err
	}

	r := csv.NewReader(f)
	_, err = r.Read()
	if err != nil {
		return err
	}

	var gm GradeMap
	for {
		rec, err := r.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}

		points, err := strconv.ParseFloat(rec[0], 32)
		if err != nil {
			return err
		}

		grade, err := strconv.ParseFloat(rec[1], 32)
		if err != nil {
			return err
		}

		gm = append(gm, GradeMapEntry{
			Points: float32(points),
			Grade:  float32(grade),
		})
	}

	g.MoodleMap[moodle.AssignId(id)] = gm
	return nil
}
