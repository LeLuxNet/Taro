package main

import (
	"archive/zip"
	"bytes"
	"context"
	"encoding/base64"
	"fmt"
	"image/jpeg"
	"image/png"
	"io"
	"log"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	ghtml "golang.org/x/net/html"
	"golang.org/x/net/html/atom"
	"lelux.net/taro"
	"lelux.net/taro/cafeteria"
	"lelux.net/taro/cafeteria/thueringen"
	"lelux.net/taro/friedolin"
	"lelux.net/taro/meinjena"
	"lelux.net/taro/moodle"
	"lelux.net/x/barcode"
	"lelux.net/x/barcode/aztec"
)

type Event struct {
	friedolin.Event

	Schedule       Schedule
	Module         friedolin.EventModule
	Moodle         moodle.CourseId
	DownloadCenter []moodle.DownloadSection
}

type Module struct {
	friedolin.Module

	Events []friedolin.SearchEvent
}

type Grades struct {
	Terms   []GradeTerm
	Credits uint8
	Grade   float32
}

type GradeTerm struct {
	Term    friedolin.Term
	Credits uint8
	Grade   float32
	Modules []GradeModule
}

type GradeModule struct {
	friedolin.GradeModule
	Exams []GradeExam
}

type GradeExam struct {
	friedolin.GradeExam
	Points      *float32
	PointsParts []float32
	Name        string
	Tries       uint8
}

type Room struct {
	friedolin.Room
	Building friedolin.Building
}

type Search struct {
	Name   string
	Events []friedolin.SearchEvent
}

type TicketLogin struct {
	Email    string
	Password string
	Tickets  []Ticket
}

type Ticket struct {
	Name               string
	SubscriptionNumber string
	Format             string
	Code               string
}

type CafeteriaResult struct {
	Date       taro.Date
	DateBefore taro.Date
	DateAfter  taro.Date
	Meals      [4][]CafeteriaMeal
}

type Cafeteria struct {
	Name string
	cafeteria.Cafeteria
}

type CafeteriaMeal struct {
	Name     string
	Optional bool
	Dishes   []cafeteria.Dish
}

type News struct {
	Source  string          `json:"source"`
	Id      string          `json:"id,omitempty"`
	Title   string          `json:"title"`
	Link    string          `json:"link,omitempty"`
	Date    taro.Date       `json:"date"`
	Body    *friedolin.HTML `json:"body"`
	Partial bool            `json:"partial"`
}

type ApiLogin struct {
	Session string `json:"session"`
}

var fsrInfoRss = NewRss("fsrInformatik", "https://meinfsr.de/index.xml")

func news(f *friedolin.Fridolin, m *moodle.Moodle) []News {
	maxYear, maxMonth, maxDay := time.Now().Add(-90 * 24 * time.Hour).Date()
	maxAge := taro.Date{Year: uint16(maxYear), Month: maxMonth, Day: uint8(maxDay)}

	var wg sync.WaitGroup
	wg.Add(3)

	var friedolinNews []friedolin.News
	go func() {
		defer wg.Done()
		var err error
		friedolinNews, err = f.News()
		if err != nil {
			log.Println("failed to fetch friedolin news:", err)
			return
		}
	}()

	var moodleConversations []moodle.Conversation
	go func() {
		defer wg.Done()
		if m != nil && m.Session != "" {
			var err error
			moodleConversations, err = m.Conversations()
			if err != nil {
				log.Println("failed to fetch moodle conversations:", err)
				return
			}
		}
	}()

	var news []News
	go func() {
		defer wg.Done()
		news = fsrInfoRss.News(maxAge)
	}()

	wg.Wait()

	for _, n := range friedolinNews {
		if n.Date.Before(maxAge) {
			break
		}
		news = append(news, News{
			Source: "friedolin",
			Title:  n.Title,
			Date:   n.Date,
			Body:   n.Body,
		})
	}

	for _, c := range moodleConversations {
		for _, m := range c.Messages {
			year, month, day := time.Time(m.TimeCreated).Date()
			date := taro.Date{
				Year:  uint16(year),
				Month: month,
				Day:   uint8(day),
			}

			n := ghtml.Node{Type: ghtml.ElementNode, Data: "div", DataAtom: atom.Div}
			nodes, err := ghtml.ParseFragment(strings.NewReader(m.Text), &n)
			if err != nil {
				log.Printf("failed to parse moodle message text: %s", err)
			} else {
				for _, child := range nodes {
					n.AppendChild(child)
				}

				news = append(news, News{
					Source: "moodle",
					Id:     strconv.FormatUint(uint64(m.Id), 10),
					Title:  c.Title(),
					Date:   date,
					Body:   (*friedolin.HTML)(n.FirstChild),
				})
			}
		}
	}

	sort.Slice(news, func(i, j int) bool {
		return news[i].Date.After(news[j].Date)
	})

	return news
}

func main() {
	err := LoadSettings()
	if err != nil {
		log.Println("failed to load settings:", err)
	}

	mTz, err := time.LoadLocation("Europe/Berlin")
	if err != nil {
		log.Fatalf("failed to load timezone 'Europe/Berlin': %s", err)
	}
	m := moodle.Moodle{
		URL:      "https://moodle.uni-jena.de",
		Timezone: mTz,
	}

	Route("", 0, nil, func(ctx context.Context, s *Session, ids []uint) (interface{}, error) {
		return news(&s.Fridolin, &s.Moodle), nil
	})

	ApiRoute(true, "login", false, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		err := f.Login(values.Get("username"), values.Get("password"))
		return ApiLogin{f.Session}, err
	})
	Route("login", 0, func(s *Session, ids []uint, get, post url.Values) (string, interface{}, error) {
		username := post.Get("username")
		password := post.Get("password")

		err := s.Fridolin.Login(username, password)
		if err != nil {
			return "", nil, err
		}
		s.LoggedIn = true

		go func() {
			s.Moodle = m
			err = s.Moodle.LoginShibboleth(username, password)
			if err != nil {
				log.Println("failed moodle login:", err)
			}
		}()

		s.bootstrap()

		redirect := get.Get("redirect")
		if !strings.HasPrefix(redirect, "/") {
			redirect = "/schedule"
		}

		return redirect, nil, nil
	}, nil)

	ApiRoute(true, "logout", false, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return nil, f.Logout()
	})
	Route("logout", 0, func(s *Session, id []uint, get, post url.Values) (string, interface{}, error) {
		s.Fridolin.Logout()
		*s = Session{}
		return "/", nil, nil
	}, nil)

	ApiRoute(false, "schedule", false, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return f.Schedule()
	})

	Route("events", 0, nil, func(ctx context.Context, s *Session, ids []uint) (interface{}, error) {
		fs, err := s.Fridolin.Schedule()
		if err != nil {
			return nil, err
		}

		rs := newSchedule(true)
		for i, e := range fs {
			for _, g := range e.Groups {
				for _, m := range g.Meetings {
					if m.Frequency == friedolin.Once {
						continue
					}

					if m.Type == 0 {
						m.Type = e.Type
					}
					rs.Add(e.Id, e.Name, i, len(fs), e.Type == friedolin.Exercise, m)
				}
			}
		}
		rs.Finish()

		return rs, nil
	})

	Route("schedule", 0, nil, func(ctx context.Context, s *Session, ids []uint) (interface{}, error) {
		fs, err := s.Fridolin.Schedule()
		if err != nil {
			return nil, err
		}

		now := time.Now()
		wd := now.Weekday()
		monday := taro.ToDate(now.AddDate(0, 0, -int(wd)))
		sunday := taro.ToDate(now.AddDate(0, 0, 7-int(wd)))

		rs := newSchedule(true)
		for i, e := range fs {
			for _, g := range e.Groups {
				for _, m := range g.Meetings {
					if m.FromDate == nil || m.FromDate.After(sunday) ||
						m.ToDate == nil || m.ToDate.Before(monday) {
						continue
					}

					if m.Type == 0 {
						m.Type = e.Type
					}
					rs.Add(e.Id, e.Name, i, len(fs), e.Type == friedolin.Exercise, m)
				}
			}
		}
		rs.Finish()

		return rs, nil
	})

	grades := LocalGrades{
		MatNumberFriedolin: make(map[uint32]map[string][]Grade),
		MoodleMap:          make(map[moodle.AssignId]GradeMap),
	}
	err = grades.Load()
	if err != nil {
		log.Fatalln("failed to load grades:", err)
	}

	ApiRoute(false, "grades", true, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return f.Grades(uint8(id))
	})
	Route("grades", 2, nil, func(ctx context.Context, s *Session, ids []uint) (interface{}, error) {
		points := ids[0] != 0

		fg, err := s.Fridolin.Grades(uint8(ids[1]))
		if err != nil {
			return nil, err
		}

		tries := make(map[uint32]uint8)
		for _, fs := range fg {
			for _, fm := range fs.Modules {
				for _, fe := range fm.Exams {
					if n, ok := tries[fe.Number]; !ok || n < fe.Try {
						tries[fe.Number] = fe.Try
					}
				}
			}
		}

		g := make(map[friedolin.Term]GradeTerm)
		for _, fs := range fg {
			for _, fg := range fs.Modules {
				var e []GradeExam

				if points {
					if sm, ok := grades.MatNumberFriedolin[s.Info.MatriculationNumber]; ok {
						for _, se := range sm[fg.ShortText] {
							sum := se.Sum
							e = append(e, GradeExam{
								GradeExam:   friedolin.GradeExam{Reason: se.Reason},
								Points:      &sum,
								PointsParts: se.Parts,
								Name:        se.Name,
								Tries:       1,
							})
						}
					}

					var mCourse moodle.CourseId
					switch fg.ShortText {
					case "FMI-IN0075":
						mCourse = 54119
					case "FMI-IN0001":
						mCourse = 54198
					case "FMI-IN0005":
						mCourse = 58556
					case "FMI-IN0170":
						mCourse = 58574
					case "FMI-MA0007":
						mCourse = 57832
					case "FMI-IN0144":
						mCourse = 58264
					}

					if s.Moodle.Session != "" && mCourse != 0 {
						gr, err := s.Moodle.GradeReport(mCourse)
						if err != nil {
							log.Printf("failed to fetch moodle grade report %d: %s", mCourse, err)
						}

						filled := false
						for i := len(gr) - 1; i >= 0; i-- {
							g := gr[i]
							if g.Assign == nil {
								continue
							}

							if g.Grade != nil {
								filled = true
							} else if filled {
								var zero float32
								gr[i].Grade = &zero
							} else {
								a, err := s.Moodle.Assign(*g.Assign)
								if err == nil && len(a.Submission) == 0 && a.Due.Before(time.Now()) {
									filled = true
									var zero float32
									gr[i].Grade = &zero
								}
							}
						}

						for _, g := range gr {
							if g.Assign == nil {
								continue
							}

							ge := GradeExam{
								Points: g.Grade,
								Name:   g.Item,
								Tries:  1,
							}

							if g.Grade != nil {
								if m, ok := grades.MoodleMap[*g.Assign]; ok {
									ge.Grade = m.Resolve(*g.Grade)
								}
							}

							e = append(e, ge)
						}
					}
				}

				for _, fe := range fg.Exams {
					name := fg.Name
					if len(fe.Units) > 0 && len(fe.Units[0].Events) > 0 {
						name2, ok := strings.CutPrefix(fe.Units[0].Events[0].Name, fg.Name+" - ")
						if ok {
							name = name2
						}
					}

					e = append(e, GradeExam{
						GradeExam: fe,
						Name:      name,
						Tries:     tries[fe.Number],
					})
				}

				gt, _ := g[fg.Term]
				gt.Credits += fg.Credits
				gt.Grade += fg.Grade * float32(fg.Credits)
				gt.Modules = append(gt.Modules, GradeModule{GradeModule: fg, Exams: e})
				g[fg.Term] = gt
			}
		}

		var credits uint8
		var grade float32

		gl := make([]GradeTerm, 0, len(g))
		for term, gt := range g {
			gt.Term = term
			credits += gt.Credits
			grade += gt.Grade
			if gt.Credits > 0 {
				gt.Grade /= float32(gt.Credits)
			}
			gl = append(gl, gt)
		}
		sort.Slice(gl, func(i, j int) bool {
			return gl[i].Term.After(gl[j].Term)
		})

		if credits > 0 {
			grade /= float32(credits)
		}

		return Grades{
			Terms:   gl,
			Credits: credits,
			Grade:   grade,
		}, nil
	})

	ApiRoute(false, "event", true, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return f.Event(friedolin.EventId(id))
	})
	Route("event", 1, nil, func(ctx context.Context, s *Session, ids []uint) (interface{}, error) {
		fe, err := s.Fridolin.Event(friedolin.EventId(ids[0]))
		if err != nil {
			return nil, err
		}
		e := Event{Event: fe}

		e.Schedule = newSchedule(false)
		for i, g := range e.Groups {
			var name string
			if g.Number == nil {
				name = "unbenannte Gruppe"
			} else {
				name = fmt.Sprintf("Gruppe %d", *g.Number)
			}

			for _, m := range g.Meetings {
				e.Schedule.Add(fe.Id, name, i, len(e.Groups), m.Frequency == friedolin.Once, m)
			}
		}
		e.Schedule.Finish()

		for _, m := range e.Modules {
			if m.ShortText == e.ShortText {
				e.Module = m
			}
		}

		for _, link := range e.Links {
			id := strings.TrimPrefix(link.Link, "https://moodle.uni-jena.de/course/view.php?id=")
			if len(id) != len(link.Link) {
				id, err := strconv.ParseUint(id, 10, 32)
				if err != nil {
					log.Printf("failed to parse moodle link '%s'", link.Link)
				} else {
					e.Moodle = moodle.CourseId(id)
				}
			}
		}
		if s.Moodle.Session != "" {
			if e.Moodle == 0 {
				idStr := strconv.FormatUint(uint64(e.Id), 10)
				mSearch, err := s.Moodle.CourseSearch(idStr)
				if err != nil {
					log.Printf("failed to search for course %s on moodle", idStr)
				} else if len(mSearch) > 0 {
					e.Moodle = mSearch[0].Id
				}
			}
			if e.Moodle != 0 {
				e.DownloadCenter, err = s.Moodle.DownloadCenter(e.Moodle)
				if err != nil {
					log.Printf("failed to fetch download center %d: %s", e.Moodle, err)
				}
			}
		}

		return e, nil
	})
	Route("event_file", 4, nil, func(ctx context.Context, s *Session, ids []uint) (interface{}, error) {
		typ := moodle.DownloadType(ids[2])
		zipR, err := s.Moodle.DownloadCenterDownload(moodle.CourseId(ids[0]), ids[1], typ, ids[3])
		if err != nil {
			return nil, err
		}

		if typ == moodle.DownloadFolder {
			return typ, nil
		}

		b, err := io.ReadAll(zipR)
		if err != nil {
			return nil, err
		}

		zip, err := zip.NewReader(bytes.NewReader(b), int64(len(b)))
		if err != nil {
			return nil, err
		}
		if len(zip.File) != 1 {
			return bytes.NewReader(b), nil
		}

		return zip.File[0].Open()
	})

	Route("ticket", 0, func(s *Session, ids []uint, get, post url.Values) (string, interface{}, error) {
		email := post.Get("email")
		password := post.Get("password")

		var mj meinjena.MeinJena
		err := mj.Login(email, password)
		if err != nil {
			return "", nil, err
		}

		var handyTickets []meinjena.HandyTicket

		snS := post.Get("subNumber")
		if snS == "" {
			sm, err := mj.SyncMain()
			if err != nil {
				return "", nil, err
			}

			handyTickets = sm.HandyTickets
		} else {
			sn, err := strconv.ParseUint(snS, 10, 32)
			if err != nil {
				return "", nil, err
			}

			day, err := strconv.ParseUint(post.Get("day"), 10, 8)
			if err != nil {
				return "", nil, err
			}
			month, err := strconv.ParseUint(post.Get("month"), 10, 8)
			if err != nil {
				return "", nil, err
			}
			year, err := strconv.ParseUint(post.Get("year"), 10, 16)
			if err != nil {
				return "", nil, err
			}

			date := taro.Date{
				Day:   uint8(day),
				Month: time.Month(month),
				Year:  uint16(year),
			}

			handyTickets, err = mj.AddHandyticket(uint(sn), date)
			if err != nil {
				return "", nil, err
			}
		}

		now := time.Now()

		tickets := make([]Ticket, 0, len(handyTickets))
		for _, ticket := range handyTickets {
			if now.Before(time.Time(ticket.ValidFrom)) || now.After(time.Time(ticket.ValidUntil)) {
				continue
			}

			uid, err := strconv.ParseUint(ticket.Uid, 10, 32)
			if err != nil {
				log.Println("failed to parse ticket uid:", err)
				continue
			}

			r, err := mj.HandyticketBarcode(uint(uid))
			if err != nil {
				log.Printf("failed to fetch ticket %d: %s", uid, err)
				continue
			}

			format := "jpeg"
			b, err := io.ReadAll(r)
			r.Close()
			if err != nil {
				log.Printf("failed to read ticket %d: %s", uid, err)
			}

			b2, err := ReencodeTicket(b)
			if err != nil {
				log.Println("failed to reencode ticket:", err)
			} else {
				format = "png"
				b = b2
			}

			tickets = append(tickets, Ticket{
				Name:               ticket.ProductName,
				SubscriptionNumber: ticket.AboNr,
				Format:             format,
				Code:               base64.StdEncoding.EncodeToString(b),
			})
		}

		return "", TicketLogin{
			Email:    email,
			Password: password,
			Tickets:  tickets,
		}, nil
	}, func(ctx context.Context, s *Session, ids []uint) (interface{}, error) {
		return TicketLogin{}, nil
	})

	ApiRoute(false, "module", true, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return f.Module(friedolin.ModuleId(id))
	})
	Route("module", 1, nil, func(ctx context.Context, s *Session, ids []uint) (interface{}, error) {
		fm, err := s.Fridolin.Module(friedolin.ModuleId(ids[0]))
		if err != nil {
			return nil, err
		}
		m := Module{Module: fm}

		search, err := s.Fridolin.SearchEvents(friedolin.EventSearch{
			Name: fm.Name,
			Term: friedolin.Term{Season: friedolin.Summer, Year: 2024},
		}, 10, 0)
		if err != nil {
			return nil, err
		}

		for _, e := range search {
			if e.Name == fm.Name {
				m.Events = append(m.Events, e)
			}
		}

		return m, nil
	})

	ApiRoute(false, "room", true, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return f.Room(friedolin.RoomId(id))
	})
	Route("room", 1, nil, func(ctx context.Context, s *Session, ids []uint) (interface{}, error) {
		r, err := s.Fridolin.Room(friedolin.RoomId(ids[0]))
		if err != nil {
			return nil, err
		}

		b, err := s.Fridolin.Building(r.Building.Id)
		if err != nil {
			log.Printf("failed to load building '%d': %s", r.Building.Id, err)
		}

		return Room{r, b}, nil
	})

	ApiRoute(false, "buildings", false, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return f.Buildings()
	})
	ApiRoute(false, "building", true, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return f.Building(friedolin.BuildingId(id))
	})

	ApiRoute(false, "myInfo", false, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return f.MyInfo()
	})

	ApiRoute(false, "paymentInformation", false, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return f.PaymentInformation()
	})

	ApiRoute(false, "news", false, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return news(&f, nil), nil
	})

	ApiRoute(false, "friedolin/news", false, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		return f.News()
	})

	Route("search", 0, func(s *Session, ids []uint, get, post url.Values) (string, interface{}, error) {
		name := post.Get("name")

		var events []friedolin.SearchEvent
		var err error
		if name != "" {
			events, err = s.Fridolin.SearchEvents(friedolin.EventSearch{Name: name}, 10, 0)
		}

		return "", Search{name, events}, err
	}, nil)

	ApiRoute(false, "cafeteria", true, func(f friedolin.Fridolin, id uint, values url.Values) (interface{}, error) {
		var d taro.Date
		dateStr := values.Get("date")
		if dateStr != "" {
			_ = d.UnmarshalText([]byte(dateStr))
		}
		if d == (taro.Date{}) {
			d = taro.Today()
		}

		c := thueringen.Thüringen{Id: uint8(id)}
		return c.Meals(context.Background(), d)
	})

	cs := []Cafeteria{
		{"Ernst-Abbe-Platz", thueringen.Jena_ErstAbbePlatz},
		{"Philosophenweg", thueringen.Jena_Philosophenweg},
		{"Uni Hauptgebäude", thueringen.Jena_UniHauptgebäude},
		{"Zur Rosen", thueringen.Jena_ZurRosen},
	}
	Route("cafeteria", 0, nil, func(ctx context.Context, s *Session, ids []uint) (interface{}, error) {
		var d taro.Date
		if len(ids) == 3 {
			d.Year = uint16(ids[0])
			d.Month = time.Month(ids[1])
			d.Day = uint8(ids[2])
		} else {
			t := time.Now()
			if t.Hour() > 22 {
				t = t.AddDate(0, 0, 1)
			}
			d = taro.ToDate(t)
		}

		var wg sync.WaitGroup
		meals := make([][]cafeteria.Meal, len(cs))
		for i, c := range cs {
			wg.Add(1)
			go func(i int, c Cafeteria) {
				defer wg.Done()
				var err error
				meals[i], err = c.Meals(ctx, d)
				if err != nil {
					log.Printf("failed to fetch mensa '%s': %s", c.Name, err)
				}
			}(i, c)
		}
		wg.Wait()

		var res [4][]CafeteriaMeal
		for i, c := range cs {
			for _, m := range meals[i] {
				if c.Cafeteria == thueringen.Jena_ZurRosen {
					m.Time = cafeteria.Evening
				}

				res[m.Time-1] = append(res[m.Time-1], CafeteriaMeal{
					Name:     c.Name,
					Optional: c.Cafeteria == thueringen.Jena_ZurRosen,
					Dishes:   m.Dishes,
				})
			}
		}

		return CafeteriaResult{
			Date:       d,
			DateBefore: d.AddDate(0, 0, -1),
			DateAfter:  d.AddDate(0, 0, 1),
			Meals:      res,
		}, nil
	})

	log.Println("listening")
	log.Fatalln(http.ListenAndServe(":8080", nil))
}

func ReencodeTicket(b []byte) ([]byte, error) {
	img, err := jpeg.Decode(bytes.NewReader(b))
	if err != nil {
		return nil, err
	}

	matrix, err := barcode.GlobalHistogramBinarizer(img).BlackMatrix()
	if err != nil {
		return nil, err
	}

	det, err := aztec.Detect(matrix, false)
	if err != nil {
		return nil, err
	}

	var buf bytes.Buffer
	err = png.Encode(&buf, det.Bits)
	return buf.Bytes(), err
}
