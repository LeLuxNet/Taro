package main

import (
	"sort"
	"time"

	"lelux.net/taro/friedolin"
)

func newSchedule(eventLink bool) Schedule {
	return Schedule{
		EventLink: eventLink,
		Days:      make([]ScheduleDay, 5),
		MinTime:   10 * 60,
		MaxTime:   14 * 60,
	}
}

type Schedule struct {
	EventLink bool
	Labels    []ScheduleLabel
	Days      []ScheduleDay
	MinTime   uint
	MaxTime   uint
}

type ScheduleLabel struct {
	From uint
	Hour uint
}

type ScheduleDay []ScheduleBlock

func (d ScheduleDay) Find(from, to uint) int {
	for i, block := range d {
		if from < block.To && to > block.From {
			return i
		}
	}
	return -1
}

func (d *ScheduleDay) Add(m ScheduleMeeting) {
	i := d.Find(m.From, m.To)
	if i == -1 {
		*d = append(*d, ScheduleBlock{
			From: m.From,
			To:   m.To,
			Cols: []ScheduleCols{{m}},
		})
	} else {
		[]ScheduleBlock(*d)[i].Add(m)
	}
}

type ScheduleBlock struct {
	From uint
	To   uint

	Cols []ScheduleCols
}

func (b *ScheduleBlock) Add(m ScheduleMeeting) {
	if b.From > m.From {
		b.From = m.From
	}
	if b.To < m.To {
		b.To = m.To
	}

	for i, col := range b.Cols {
		if col.Allows(m.From, m.To) {
			b.Cols[i] = append(col, m)
			return
		}
	}
	b.Cols = append(b.Cols, []ScheduleMeeting{m})
}

type ScheduleCols []ScheduleMeeting

func (c ScheduleCols) Allows(from, to uint) bool {
	for _, m := range c {
		if from <= m.To && to >= m.From {
			return false
		}
	}
	return true
}

type ScheduleMeeting struct {
	friedolin.Meeting
	EventId friedolin.EventId
	Name    string
	Striped bool
	Hash    float64

	From uint
	To   uint
}

func timeToUint(t friedolin.Time) uint {
	return uint(t.Hour)*60 + uint(t.Minute)
}

func (s *Schedule) Add(id friedolin.EventId, name string, index int, count int, striped bool, meeting friedolin.Meeting) {
	if meeting.State == friedolin.Cancelled {
		return
	}
	if meeting.FromTime == nil || meeting.ToTime == nil {
		return
	}

	m := ScheduleMeeting{
		Meeting: meeting,
		EventId: id,
		Name:    name,
		Striped: striped,
		From:    timeToUint(*meeting.FromTime),
		To:      timeToUint(*meeting.ToTime),
	}

	m.Hash = float64(index) / float64(count)

	if m.Online && m.Room == nil {
		m.Room = &friedolin.MeetingRoom{Name: "Online"}
	}

	if s.MinTime > m.From {
		s.MinTime = m.From
	}
	if s.MaxTime < m.To {
		s.MaxTime = m.To
	}

	weekday := (m.Weekday - time.Monday) % 7
	s.Days[weekday].Add(m)
}

func (s *Schedule) Finish() {
	for h := s.MinTime / 60; h <= s.MaxTime/60; h++ {
		l := ScheduleLabel{From: (h * 60) - s.MinTime, Hour: h}
		s.Labels = append(s.Labels, l)
	}

	for _, day := range s.Days {
		for i, block := range day {
			for _, col := range block.Cols {
				for i := range col {
					col[i].From -= block.From
					col[i].To -= block.From
				}
				sort.Slice(col, func(i, j int) bool {
					return col[i].From < col[j].From
				})
			}

			sort.SliceStable(block.Cols, func(i, j int) bool {
				return block.Cols[i][0].From < block.Cols[j][0].From
			})

			day[i].From -= s.MinTime
			day[i].To -= s.MinTime
		}
	}
	s.MaxTime -= s.MinTime
	s.MinTime = 0
}
