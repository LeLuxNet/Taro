package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"sync"

	"github.com/SlyMarbo/rss"
	ghtml "golang.org/x/net/html"
	"golang.org/x/net/html/atom"
	"lelux.net/taro"
	"lelux.net/taro/friedolin"
	"lelux.net/x/html"
)

type Rss struct {
	Source string
	URL    string

	pageCache     map[string]*friedolin.HTML
	pageCacheLock sync.RWMutex
}

func NewRss(source, url string) *Rss {
	return &Rss{
		Source:    source,
		URL:       url,
		pageCache: make(map[string]*friedolin.HTML),
	}
}

func (r *Rss) News(maxAge taro.Date) []News {
	items, err := rss.Fetch(r.URL)
	if err != nil {
		log.Printf("failed to fetch %s news: %s", r.Source, err)
		return nil
	}

	var news []News
	var newsLock sync.Mutex

	var wg sync.WaitGroup

	for _, item := range items.Items {
		year, month, day := item.Date.Date()
		date := taro.Date{
			Year:  uint16(year),
			Month: month,
			Day:   uint8(day),
		}
		if date.Before(maxAge) {
			break
		}

		r.pageCacheLock.RLock()
		body, ok := r.pageCache[item.ID]
		r.pageCacheLock.RUnlock()

		wg.Add(1)
		go func(item *rss.Item) {
			defer wg.Done()
			if !ok {
				newBody, err := r.freshBody(item.Link)
				if err != nil {
					log.Println(err)
				} else {
					body = newBody
					ok = true

					r.pageCacheLock.Lock()
					r.pageCache[item.ID] = body
					r.pageCacheLock.Unlock()
				}
			}

			partial := !ok
			if partial {
				n := ghtml.Node{Type: ghtml.ElementNode, Data: "div", DataAtom: atom.Div}
				nodes, err := ghtml.ParseFragment(strings.NewReader(item.Summary), &n)
				if err != nil {
					log.Printf("failed to parse %s news content: %s", r.Source, err)
					return
				}

				for _, child := range nodes {
					n.AppendChild(child)
				}
				body = (*friedolin.HTML)(n.FirstChild)
			}

			newsLock.Lock()
			news = append(news, News{
				Source:  r.Source,
				Id:      item.ID,
				Title:   item.Title,
				Link:    item.Link,
				Date:    date,
				Body:    body,
				Partial: partial,
			})
			newsLock.Unlock()
		}(item)
	}

	wg.Wait()

	return news
}

var article = html.MustCompile("article")

func (r *Rss) freshBody(url string) (*friedolin.HTML, error) {
	log.Printf("fresh page fetch %s: '%s'", r.Source, url)

	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	n, err := html.Parse(res.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to parse '%s': %w", url, err)
	}
	article := n.Query(article)

	for c := article.FirstChild; c != nil; c = c.NextSibling {
		if c.DataAtom == atom.Link {
			c.PrevSibling.NextSibling = c.NextSibling
			if c.NextSibling != nil {
				c.NextSibling.PrevSibling = c.PrevSibling
			}
		}

		class := (*html.Node)(c).GetAttr("class")
		if class == "blog-tags" {
			c.PrevSibling.NextSibling = nil
			break
		}
	}

	return friedolin.CleanHtml(article.FirstChild, res.Request.URL), nil
}
