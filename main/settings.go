package main

import (
	"encoding/json"
	"errors"
	"log"
	"os"
	"sync"
)

type Setting struct {
	Name string `json:"name,omitempty"`
}

var settings map[string]*Setting
var settingsLock sync.Mutex

const settingsPath = "settings.json"

func LoadSettings() error {
	b, err := os.ReadFile(settingsPath)
	switch {
	case errors.Is(err, os.ErrNotExist):
		settings = make(map[string]*Setting)
		return nil
	case err == nil:
		return json.Unmarshal(b, &settings)
	default:
		return err
	}
}

func SaveSettings() {
	defer settingsLock.Unlock()

	b, err := json.MarshalIndent(settings, "", "\t")
	if err != nil {
		log.Println("failed to marshal settings:", err)
		return
	}

	err = os.WriteFile(settingsPath, b, 0600)
	if err != nil {
		log.Println("failed to save settings:", err)
	}
}
