package main

import (
	"lelux.net/taro/cafeteria"
)

type DietItem struct {
	Emoji       string
	Description string
}

func dietInfo(d cafeteria.Diet) []DietItem {
	var items []DietItem

	if d&cafeteria.Fish != 0 {
		items = append(items, DietItem{Emoji: "🐟", Description: "enthält Fisch"})
	}
	if d&cafeteria.Pork != 0 {
		items = append(items, DietItem{Emoji: "🥓", Description: "enthält Schweinefleisch"})
	}
	if d&cafeteria.Beef != 0 {
		items = append(items, DietItem{Emoji: "🥩", Description: "enthält Rindfleisch"})
	}
	if d&cafeteria.Poultry != 0 {
		items = append(items, DietItem{Emoji: "🍗", Description: "enthält Geflügel"})
	}
	if d&cafeteria.Crustacean != 0 {
		items = append(items, DietItem{Emoji: "🦀", Description: "enthält Krebstiere"})
	}
	if d&cafeteria.AnimalGelatin != 0 {
		items = append(items, DietItem{Emoji: "🧫", Description: "enthält Gelatine"})
	}
	if d&cafeteria.AnimalRennet != 0 {
		items = append(items, DietItem{Emoji: "🧪", Description: "enthält tierisches Lab"})
	}
	if d&cafeteria.Carmine != 0 {
		items = append(items, DietItem{Emoji: "🩸", Description: "enthält Karmin"})
	}
	if d&cafeteria.Vegetarian == 0 && len(items) == 0 {
		items = append(items, DietItem{Emoji: "🍖", Description: "enthält Fleisch"})
	}

	if d&cafeteria.Milk != 0 {
		items = append(items, DietItem{Emoji: "🥛", Description: "enthält Milch"})
	}
	if d&cafeteria.ChickenEgg != 0 {
		items = append(items, DietItem{Emoji: "🥚", Description: "enthält Ei"})
	}

	if d&cafeteria.Vegetarian != 0 {
		if d&cafeteria.Honey != 0 {
			items = append(items, DietItem{Emoji: "🍯", Description: "enthält Honig"})
		}
		if d&cafeteria.Vegan == 0 && d&cafeteria.Waxed != 0 {
			items = append(items, DietItem{Emoji: "🕯️", Description: "enthält Bienenwachs"})
		}
		if d&cafeteria.Vegan == 0 && len(items) == 0 {
			items = append(items, DietItem{Emoji: "🍳", Description: "enthält tierische Produkte"})
		}
	}

	if d&cafeteria.Caffeine != 0 {
		items = append(items, DietItem{Emoji: "☕", Description: "enthält Koffein"})
	}
	if d&cafeteria.Alcohol != 0 {
		items = append(items, DietItem{Emoji: "🍷", Description: "enthält Alkohol"})
	}
	if d&cafeteria.Peanut != 0 {
		items = append(items, DietItem{Emoji: "🥜", Description: "enthält Erdnuss"})
	}
	if d&cafeteria.Gluten != 0 {
		items = append(items, DietItem{Emoji: "🌾", Description: "enthält Gluten"})
	}

	if d&cafeteria.Organic != 0 {
		items = append(items, DietItem{Emoji: "💚", Description: "BIO"})
	}

	return items
}
