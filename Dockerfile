FROM golang:1.21-alpine AS build

WORKDIR /app

COPY go.* ./
RUN go mod download

COPY . .
RUN go build -o /out ./main

FROM alpine

RUN apk add --no-cache tzdata

WORKDIR /

COPY --from=build /out /out

WORKDIR /app

ENTRYPOINT ["/out"]
