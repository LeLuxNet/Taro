package meinjena

import (
	"fmt"
	"io"
	"net/http"
	"strconv"
	"time"

	"lelux.net/taro"
)

const timeFormat = "2006-01-02T15:04:05-0700"

type Time time.Time

func (t *Time) UnmarshalText(b []byte) error {
	t2, err := time.Parse(timeFormat, string(b))
	*t = Time(t2)
	return err
}

func (t Time) MarshalText() ([]byte, error) {
	return []byte(time.Time(t).Format(timeFormat)), nil
}

type HandyTicket struct {
	Uid         string
	AboNr       string
	ProductName string
	ValidFrom   Time
	ValidUntil  Time
}

type changeHandyTicketReq struct {
	AboNr     string `json:"aboNr"`
	BirthDate string `json:"birthDate,omitempty"`
}

type addHandyTicketRes struct {
	HandyTickets []HandyTicket
}

func (m *MeinJena) AddHandyticket(aboNr uint, birthDate taro.Date) ([]HandyTicket, error) {
	var res addHandyTicketRes
	err := m.req(http.MethodPost, "handyticket", changeHandyTicketReq{
		AboNr:     strconv.FormatUint(uint64(aboNr), 10),
		BirthDate: birthDate.Format("02.01.2006"),
	}, &res)
	return res.HandyTickets, err
}

func (m *MeinJena) DeleteHandyticket(aboNr uint) error {
	return m.req(http.MethodDelete, "handyticket", changeHandyTicketReq{
		AboNr: strconv.FormatUint(uint64(aboNr), 10),
	}, nil)
}

func (m *MeinJena) HandyticketBarcode(uid uint) (io.ReadCloser, error) {
	var res io.ReadCloser
	err := m.req(http.MethodGet, fmt.Sprintf("handyticket/%d/barcode", uid), nil, &res)
	return res, err
}
