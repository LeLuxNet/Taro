package meinjena

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"time"

	xhttp "lelux.net/x/net/http"
)

type MeinJena struct {
	AuthToken    string
	RefreshToken string
}

type Error struct {
	Message          string
	ErrorCode        string
	DebugInformation string
	Data             json.RawMessage
}

func (e Error) Error() string {
	if e.ErrorCode != "" {
		return fmt.Sprintf("meinjena: %s (%s) %s", e.Message, e.ErrorCode, e.Data)
	} else {
		return fmt.Sprintf("meinjena: %s %s", e.Message, e.Data)
	}
}

func (m *MeinJena) req(method, path string, req, res interface{}) error {
	var body io.Reader
	if req != nil {
		b, err := json.Marshal(req)
		if err != nil {
			return err
		}
		body = bytes.NewReader(b)
	}

	reqH, err := http.NewRequest(method, "https://www.meinjena.de/api/v1/"+path, body)
	if err != nil {
		return err
	}

	reqH.Header.Set("Content-Type", "application/json")
	reqH.Header.Set("Api-Key", "eb4Hw8mxRcoAKm1ho6rLHl6Epf9xDXe8")
	reqH.Header.Set("Device-ID", "7bf0cefe704ce85c340a2432426467bd")
	reqH.Header.Set("Device-Type", "")
	reqH.Header.Set("Bundle-Version", "")
	reqH.Header.Set("Build-Version", "")
	reqH.Header.Set("Timestamp", strconv.FormatInt(time.Now().UnixMilli(), 10))
	if m.AuthToken != "" {
		reqH.Header.Set("Auth-Token", m.AuthToken)
	}

	resH, err := http.DefaultClient.Do(reqH)
	if err != nil {
		return err
	}

	if resH.StatusCode != http.StatusOK {
		b, err := io.ReadAll(resH.Body)
		resH.Body.Close()
		if err != nil {
			return err
		}

		var bodyError Error
		err = json.Unmarshal(b, &bodyError)
		if err == nil {
			return bodyError
		}

		fmt.Println("errRes", string(b))

		return xhttp.UnexpectedStatus(resH.Status)
	}

	if res == nil {
		resH.Body.Close()
		return nil
	}

	if rc, ok := res.(*io.ReadCloser); ok {
		*rc = resH.Body
		return nil
	}

	defer resH.Body.Close()
	b, err := io.ReadAll(resH.Body)
	if err != nil {
		return err
	}

	return json.Unmarshal(b, res)
}

type loginReq struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type loginRes struct {
	AuthToken    string
	RefreshToken string
}

func (m *MeinJena) Login(username, password string) error {
	var res loginRes
	err := m.req(http.MethodPost, "login", loginReq{username, password}, &res)
	if err != nil {
		return err
	}

	m.AuthToken = res.AuthToken
	m.RefreshToken = res.RefreshToken

	return nil
}

type refreshReq struct {
	RefreshToken string `json:"refreshToken"`
}

func (m *MeinJena) Refresh() error {
	var res loginRes
	err := m.req(http.MethodPost, "refresh", refreshReq{m.RefreshToken}, &res)
	if err != nil {
		return err
	}

	m.AuthToken = res.AuthToken
	m.RefreshToken = res.RefreshToken

	return nil
}

type syncMainReq struct{}

type SyncMain struct {
	HandyTickets []HandyTicket
}

func (m *MeinJena) SyncMain() (SyncMain, error) {
	var res SyncMain
	err := m.req(http.MethodPost, "sync/main", syncMainReq{}, &res)
	return res, err
}
