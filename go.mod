module lelux.net/taro

go 1.21.8

require (
	github.com/SlyMarbo/rss v1.0.5
	golang.org/x/net v0.26.0
	lelux.net/x v0.0.0-20241011111040-c09411c26dfb
)

require (
	github.com/andybalholm/cascadia v1.3.2 // indirect
	github.com/axgle/mahonia v0.0.0-20180208002826-3358181d7394 // indirect
	github.com/makiuchi-d/gozxing v0.1.1 // indirect
	golang.org/x/text v0.16.0 // indirect
	golang.org/x/xerrors v0.0.0-20240903120638-7835f813f4da // indirect
)
