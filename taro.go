package taro

import "time"

type Date struct {
	Day   uint8      `json:"day"`
	Month time.Month `json:"month"`
	Year  uint16     `json:"year"`
}

func ToDate(t time.Time) Date {
	y, m, d := t.Date()
	return Date{Day: uint8(d), Month: m, Year: uint16(y)}
}

func Today() Date {
	return ToDate(time.Now())
}

func ParseDate(layout, value string) (Date, error) {
	t, err := time.Parse(layout, value)
	if err != nil {
		return Date{}, err
	}
	return ToDate(t), nil
}

func (d Date) ToTime() time.Time {
	return time.Date(int(d.Year), d.Month, int(d.Day), 0, 0, 0, 0, time.UTC)
}

func (d Date) Format(layout string) string {
	return d.ToTime().Format(layout)
}

func (d Date) AddDate(year, month, day int) Date {
	return ToDate(d.ToTime().AddDate(year, month, day))
}

func (d Date) MarshalText() ([]byte, error) {
	return []byte(d.Format(time.DateOnly)), nil
}

func (d *Date) UnmarshalText(b []byte) error {
	var err error
	*d, err = ParseDate(time.DateOnly, string(b))
	return err
}

func (d Date) Before(d2 Date) bool {
	return d.Year < d2.Year || (d.Year == d2.Year && (d.Month < d2.Month || (d.Month == d2.Month && d.Day < d2.Day)))
}

func (d Date) After(d2 Date) bool {
	return d.Year > d2.Year || (d.Year == d2.Year && (d.Month > d2.Month || (d.Month == d2.Month && d.Day > d2.Day)))
}
